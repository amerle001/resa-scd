import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginView from "./components/public/LoginView";
import Accueil from "./components/public/Accueil";
import RegisterView from "./components/public/RegisterView";
import Admin from "./components/admin/Admin";
import UINavbar from "./components/public/UINavbar";
import UserView from "./components/user/UserView";
import UserBase from "./components/admin/UserBase";
import ProgramResa from "./components/user/ProgramResa";
import UserProgramView from "./components/user/UserProgramView";
import AdminProgramView from "./components/admin/AdminProgramView";
import Configuration from "./components/admin/Configuration";

function App() {
  return (
    <Router>
      <div className="App">
        <UINavbar />
        <Switch>
          <Route path="/" exact component={Accueil} />
          <Route path="/login" exact component={LoginView} />
          <Route path="/register" exact component={RegisterView} />
          <Route path="/admin" exact component={Admin} />
          <Route path="/userView" exact component={UserView} />
          <Route path="/userBase" exact component={UserBase} />
          <Route path="/userProgramView" exact component={UserProgramView} />
          <Route path="/adminProgramView" exact component={AdminProgramView} />
          <Route path="/configuration" exact component={Configuration} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
