import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

import { selectUser } from "../admin/userSlice";
import ProgramResa from "./ProgramResa";
import UserDashResaControl from "./UserDashResaControl";

export default function UserProgramView() {
  const userData = useSelector(selectUser);
  const history = useHistory();

  useEffect(() => {
    if (!userData) {
      setTimeout(() => {
        history.push("/");
      }, 2000);
    }
  }, []);

  return (
    <div>
      {userData ? (
        <div>
          <div className="admin-container">
            <div className="admin-sidebar">
              <ProgramResa />
            </div>
            <div className="admin-content">
              <h3>Mes réservations</h3>
              <UserDashResaControl />
            </div>
          </div>
        </div>
      ) : (
        <h1>Connectez-vous pour accéder à cette page.</h1>
      )}
    </div>
  );
}
