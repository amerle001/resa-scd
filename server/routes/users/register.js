const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
require("dotenv").config({ path: "../../config/.env" });

// @route     POST /users/register
// @desc      register user
// @access    public

router.post("/", (req, res) => {
  const { firstName, lastName, username, password } = req.body;
  User.findOne({ username }, async (err, doc) => {
    if (err) throw err;
    if (doc)
      res.send({
        status: "REGISTER_FAILURE",
        message: "Email déjà enregistré dans la base de données.",
      });
    if (
      !doc &&
      firstName === process.env.SUPER_ADMIN_FIRST_NAME &&
      lastName === process.env.SUPER_ADMIN_LAST_NAME &&
      password === process.env.SUPER_ADMIN_PASSWORD
    ) {
      const saltRounds = 10;
      const hashPassword = await bcrypt.hash(password, saltRounds);
      const newUser = new User({
        firstName,
        lastName,
        username,
        password: hashPassword,
        privilege_level: "superadmin",
        is_active: true,
      });
      await newUser
        .save()
        .then(() =>
          res.send({
            status: "REGISTER_SUCCESS",
            message: "Inscription réussie",
          })
        )
        .catch((err) =>
          res.status(500).json({ status: "REGISTER_FAILURE", message: err })
        );
    }
    if (
      !doc &&
      firstName !== process.env.SUPER_ADMIN_FIRST_NAME &&
      lastName !== process.env.SUPER_ADMIN_LAST_NAME &&
      password !== process.env.SUPER_ADMIN_PASSWORD
    ) {
      const saltRounds = 10;
      const hashPassword = await bcrypt.hash(password, saltRounds);
      const newUser = new User({
        firstName,
        lastName,
        username,
        password: hashPassword,
      });
      await newUser
        .save()
        .then(() =>
          res.send({
            status: "REGISTER_SUCCESS",
            message: "Inscription réussie",
          })
        )
        .catch((err) =>
          res.status(500).json({ status: "REGISTER_FAILURE", message: err })
        );
    }
  });
});

module.exports = router;
