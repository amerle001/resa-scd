const mongoose = require("mongoose");

const ResaLogSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now(),
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  placeNumber: {
    type: Number,
    required: true,
  },
  startPresence: {
    type: Date,
    required: true,
  },
  endPresence: {
    type: Date,
    required: true,
  },
  stayDurationInMinutes: {
    type: Number,
    required: true,
  },
});

module.exports = ResaLog = mongoose.model("resaLog", ResaLogSchema);
