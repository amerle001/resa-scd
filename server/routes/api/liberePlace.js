const express = require("express");
const router = express.Router();

// Bibliotheques Model
const Bibliotheque = require("../../models/Bibliotheque");

// @route   POST api/liberePlace
// @desc    invoque middleware de Mongoose : Model.updateOne() (findByIdAndUpdate() ne fonctionne pas sur id nichés)
// @access  PUBLIC
router.post("/", (req, res) => {
  (async () => {
    // collation des infos depuis la requête frontend
    console.log("________LIBEREPLACE________");

    const { nomOccupant, idPlace, idBib, startReservation } = req.body;
    console.log(idPlace);
    console.log(idBib);

    // *****ATTENTION******
    // logique dupliquée deleteResa
    // *****ATTENTION******

    // le test sur placeEnQuestion[0] est une sécurité
    // si jamais la requête frontend est faite sur place qui est déjà libérée par
    // la routine de libération, on se contente de renvoyer la bib telle quelle
    // sinon, le vrai processus de libération s'enclenche

    const bibEnQuestion = await Bibliotheque.findById(idBib);
    const arrayPlaces = bibEnQuestion.places;
    const placeEnQuestion = arrayPlaces.filter((place) => place._id == idPlace);

    if (placeEnQuestion[0].libre) {
      res.json(bibEnQuestion);
    } else {
      // Création d'une copie de l'état à jour à partir de l'ancien état
      const currentState = await Bibliotheque.findById(idBib).select(
        "contenance"
      );
      const ancienneContenance = currentState.contenance;
      const nouvelleContenance = ancienneContenance + 1;

      // MaJ de la db : contenance et réinitialisation de la place libérée
      await Bibliotheque.updateOne(
        {
          _id: idBib,
          "places._id": idPlace,
        },
        {
          contenance: nouvelleContenance,
          "places.$.nom": "",
          "places.$.startReservation": null,
          "places.$.endReservation": null,
          "places.$.durationInMinutes": null,
          "places.$.libre": true,
        }
      ).catch((err) => console.log(err));

      await User.findOneAndUpdate(
        { username: nomOccupant },
        { has_already_a_seat: false },
        () => console.log("has_already_a_seat: updated")
      );

      const user = await User.findOne({ username: nomOccupant });
      // const userReservations = await user.reservations;
      // attention : pour faire matcher la resa à supprimer
      // il faut à la fois vérifier que l'idPlace et le timeStamp correspondent
      // car l'user peut avoir 2 resas sur la même place, à 2 moments différents
      // donc vérifier uniquement idPlace = comportement indéfini
      if (user) {
        const userReservationToDelete = await user.reservations.filter(
          (resa) =>
            resa.placeId === idPlace &&
            new Date(resa.start).getTime() ===
              new Date(startReservation).getTime()
        );
        const userResaId = await userReservationToDelete[0]._id;

        console.log(`userResaId : ${userResaId}`);

        await user.reservations.pull({ _id: userResaId });
        await user
          .save()
          .catch((err) => console.log(err))
          .then(() => {
            console.log("Resa has been removed from user account.");
          });

        // Renvoi de la bibliothèque complète pour update Redux
        const response = await Bibliotheque.findById(idBib);
        res.json(response);
      } else {
        console.log("non possumus");

        res.json(bibEnQuestion);
      }
    }
  })();
});

module.exports = router;
