const express = require("express");
const router = express.Router();

// Bibliotheque et User Models
const Bibliotheque = require("../../models/Bibliotheque");
const User = require("../../models/User");

// @route   POST api/programResa
// @desc    programmer une réservation dans le futur !== (réserver maintenant => @route   POST api/reservePlace)
// @access  PUBLIC

router.post("/", (req, res) => {
  (async () => {
    // collation des infos de réservation à partir de la requête frontend
    let {
      nomReservant,
      idBib,
      startResa,
      endResa,
      dureeResaMinutes,
    } = req.body;

    startResa = new Date(startResa);
    endResa = new Date(endResa);

    console.log("_PROGRAM_RESA_");
    console.log("start resa");
    console.log(startResa);
    console.log("end resa");
    console.log(endResa);
    console.log("dureeResaMinutes");
    console.log(dureeResaMinutes);

    // Vérification que l'usager n'a pas deux resas concomitantes
    const user = await User.findOne({ username: nomReservant });
    for (let userFResa of user.reservations) {
      const userFResaEnd =
        userFResa.start.getTime() + userFResa.durationInMinutes * 60 * 1000;
      if (
        (startResa >= userFResa.start && endResa <= userFResaEnd) ||
        (startResa <= userFResaEnd && endResa >= userFResaEnd) ||
        (startResa <= userFResa.start && endResa >= userFResa.start) ||
        (startResa == userFResa.start && endResa == userFResaEnd)
      ) {
        console.log(
          `Programmation résa refusée : ${nomReservant} a déjà une autre resa a cet horaire.`
        );
        const response = await Bibliotheque.findById(idBib);
        res.json({
          message: `Programmation résa refusée : ${nomReservant} a déjà une autre resa a cet horaire.`,
          response,
        });
        return;
      } else if (startResa.getTime() < Date.now()) {
        console.log(
          "Programmation résa refusée : Impossible de réserver un créneau dans le passé."
        );
        const response = await Bibliotheque.findById(idBib);
        res.json({
          message: "Impossible de réserver un créneau dans le passé.",
          response,
        });
        return;
      }
    }

    const bibRequested = await Bibliotheque.findById(idBib).select("places");
    let currentSalle = await bibRequested.places;
    let currentPlaceId = null;
    let arrayFResa = [];
    console.log("dureeResaMinutes");

    // dans chaque place test de chaque array de futureResa
    // si array vide, on prend la resa
    // si array non vide et qu'une incompatibilité horaire est détectée, res.json avec pb
    // si aucun pb détecté, fResa poussée

    for (let currentPlace of currentSalle) {
      if (startResa > currentPlace.endReservation) {
        if (currentPlace.futureResa.length > 0) {
          let resaCompatible = true;
          for (let fResa of currentPlace.futureResa) {
            if (
              (startResa >= fResa.fStartReservation &&
                endResa <= fResa.fEndReservation) ||
              (startResa <= fResa.fEndReservation &&
                endResa >= fResa.fEndReservation) ||
              (startResa <= fResa.fStartReservation &&
                endResa >= fResa.fStartReservation) ||
              (startResa == fResa.fStartReservation &&
                endResa == fResa.fEndReservation)
            ) {
              console.log("break grand test overlap");
              resaCompatible = false;
              console.log("incompatibilité détectée");
            }
          }

          if (resaCompatible) {
            console.log(
              "pas d'incompatibilité, je push une resa dans currentPlace avant copie"
            );
            currentPlaceId = currentPlace._id;
            currentPlace.futureResa.push({
              nom: nomReservant,
              numero: currentPlace.numero,
              fStartReservation: startResa,
              fEndReservation: endResa,
              durationInMinutes: dureeResaMinutes,
            });
            arrayFResa = [...currentPlace.futureResa];

            await Bibliotheque.updateOne(
              {
                _id: idBib,
                "places._id": currentPlaceId,
              },
              {
                "places.$.futureResa": [...arrayFResa],
              }
            ).catch((err) => console.log(err));

            const response = await Bibliotheque.findById(idBib);
            res.json({ message: "Réservation réussie !", response });

            let idNewResa;
            for (let place of await response.places) {
              console.log("PPLLLAAACE");
              console.log(place._id.toString());
              console.log(typeof place._id);
              if (place._id.toString() === currentPlaceId.toString()) {
                console.log("MMMMAAAATCH");
                idNewResa = place.futureResa[place.futureResa.length - 1]._id;
              }
            }

            console.log("===idNewResa===");
            console.log(idNewResa);

            console.log("===currentPlaceId===");
            console.log(currentPlaceId);

            const user = await User.findOne({ username: nomReservant });
            await user.reservations.push({
              locationId: idBib,
              placeId: currentPlaceId,
              idNewResaFromPlace: idNewResa,
              locationName: response.name,
              placeNumber: currentPlace.numero,
              start: startResa,
              durationInMinutes: dureeResaMinutes,
            });
            await user
              .save()
              .then(() => console.log("Resa has been pushed to user account."));

            return;
          }
        } else {
          // il n'y a aucune réservation future dans la place, on pousse directement
          console.log(
            "il n'y a aucune réservation future dans la place, on pousse directement"
          );

          currentPlaceId = currentPlace._id;
          arrayFResa.push({
            nom: nomReservant,
            numero: currentPlace.numero,
            fStartReservation: new Date(startResa),
            fEndReservation: new Date(endResa),
            durationInMinutes: dureeResaMinutes,
          });

          await Bibliotheque.updateOne(
            {
              _id: idBib,
              "places._id": currentPlaceId,
            },
            {
              "places.$.futureResa": [...arrayFResa],
            }
          ).catch((err) => console.log(err));

          const response = await Bibliotheque.findById(idBib);
          res.json({ message: "Réservation réussie !", response });

          let idNewResa;
          for (let place of await response.places) {
            if (place._id.toString() === currentPlaceId.toString()) {
              console.log("MMMMAAAATCH");
              idNewResa = place.futureResa[place.futureResa.length - 1]._id;
            }
          }
          console.log("===idNewResa===");
          console.log(idNewResa);

          console.log("===currentPlaceId===");
          console.log(currentPlaceId);

          const user = await User.findOne({ username: nomReservant });
          await user.reservations.push({
            locationId: idBib,
            placeId: currentPlaceId,
            idNewResaFromPlace: idNewResa,
            locationName: response.name,
            placeNumber: currentPlace.numero,
            start: startResa,
            durationInMinutes: dureeResaMinutes,
          });
          await user
            .save()
            .then(() => console.log("Resa has been pushed to user account."));

          return;
        }
      } else {
        // la future réservation empiète sur la réservation immédiate
        console.log(
          "la future réservation empiète sur la réservation immédiate"
        );
      }
    }

    console.log("Désolé, il n'y a plus de place disponible à ce créneau.");

    const response = await Bibliotheque.findById(idBib);
    res.json({
      message: "Désolé, il n'y a plus de place disponible à ce créneau.",
      response,
    });
  })();
});

module.exports = router;
