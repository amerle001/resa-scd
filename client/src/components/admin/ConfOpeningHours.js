import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Popover from "react-bootstrap/Popover";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import { configureBib } from "./scdSlice";
import { unwrapResult } from "@reduxjs/toolkit";
import { useSelector, useDispatch } from "react-redux";

import { decimalToTimeStr, timeStrToDecimal } from "pretty-time-decimal";

export default function ConfOpeningHours(props) {
  const dispatch = useDispatch();
  const configureBibStatus = useSelector(
    (state) => state.scd.configureBibStatus
  );

  const bibliothequeTemp = props.scd.filter((bib) => bib._id === props.idBib);
  const bibliotheque = bibliothequeTemp[0];

  const [configRequestStatus, setConfigRequestStatus] = useState("idle");

  const [defaultResaDurationHours, setDefaultResaDurationHours] = useState(-1);
  const [openingHourMonday, setOpeningHourMonday] = useState(-1);
  const [openingHourTuesday, setOpeningHourTuesday] = useState(-1);
  const [openingHourWednesday, setOpeningHourWednesday] = useState(-1);
  const [openingHourThursday, setOpeningHourThursday] = useState(-1);
  const [openingHourFriday, setOpeningHourFriday] = useState(-1);
  const [openingHourSaturday, setOpeningHourSaturday] = useState(-1);
  const [openingHourSunday, setOpeningHourSunday] = useState(-1);
  const [closingHourMonday, setClosingHourMonday] = useState(-1);
  const [closingHourTuesday, setClosingHourTuesday] = useState(-1);
  const [closingHourWednesday, setClosingHourWednesday] = useState(-1);
  const [closingHourThursday, setClosingHourThursday] = useState(-1);
  const [closingHourFriday, setClosingHourFriday] = useState(-1);
  const [closingHourSaturday, setClosingHourSaturday] = useState(-1);
  const [closingHourSunday, setClosingHourSunday] = useState(-1);

  const canSubmit = configRequestStatus === "idle";

  const onSubmit = async (e) => {
    // e.preventDefault();
    const configBibData = {
      idBib: props.idBib,
      defaultResaDurationHours: FormatInputTimeHHMMToDecimal(
        defaultResaDurationHours
      ),
      openingHourMonday: FormatInputTimeHHMMToDecimal(openingHourMonday),
      openingHourTuesday: FormatInputTimeHHMMToDecimal(openingHourTuesday),
      openingHourWednesday: FormatInputTimeHHMMToDecimal(openingHourWednesday),
      openingHourThursday: FormatInputTimeHHMMToDecimal(openingHourThursday),
      openingHourFriday: FormatInputTimeHHMMToDecimal(openingHourFriday),
      openingHourSaturday: FormatInputTimeHHMMToDecimal(openingHourSaturday),
      openingHourSunday: FormatInputTimeHHMMToDecimal(openingHourSunday),
      closingHourMonday: FormatInputTimeHHMMToDecimal(closingHourMonday),
      closingHourTuesday: FormatInputTimeHHMMToDecimal(closingHourTuesday),
      closingHourWednesday: FormatInputTimeHHMMToDecimal(closingHourWednesday),
      closingHourThursday: FormatInputTimeHHMMToDecimal(closingHourThursday),
      closingHourFriday: FormatInputTimeHHMMToDecimal(closingHourFriday),
      closingHourSaturday: FormatInputTimeHHMMToDecimal(closingHourSaturday),
      closingHourSunday: FormatInputTimeHHMMToDecimal(closingHourSunday),
    };

    try {
      setConfigRequestStatus("loading");
      console.log(configBibData);

      const resultAction = await dispatch(configureBib(configBibData));
      unwrapResult(resultAction);
    } catch (error) {
      console.log(error);
    } finally {
      setDefaultResaDurationHours(-1);
      setOpeningHourMonday(-1);
      setOpeningHourTuesday(-1);
      setOpeningHourWednesday(-1);
      setOpeningHourThursday(-1);
      setOpeningHourFriday(-1);
      setOpeningHourSaturday(-1);
      setOpeningHourSunday(-1);
      setClosingHourMonday(-1);
      setClosingHourTuesday(-1);
      setClosingHourWednesday(-1);
      setClosingHourThursday(-1);
      setClosingHourFriday(-1);
      setClosingHourSaturday(-1);
      setClosingHourSunday(-1);
      setConfigRequestStatus("idle");
    }
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">Attention</Popover.Title>
      <Popover.Content>
        Incohérence détectée entre l'horaire d'ouverture et l'horaire de
        fermeture !
      </Popover.Content>
    </Popover>
  );

  // const checkIncoherenceHOuverture = (hOuverture, hFermeture) =>
  //   FormatInputTimeHHMMToDecimal(hOuverture) >
  //     FormatInputTimeHHMMToDecimal(hFermeture) &&
  //   hOuverture !== -1 &&
  //   FormatInputTimeHHMMToDecimal(hFermeture) !== -1 ? (
  //     <OverlayTrigger trigger="click" placement="right" overlay={popover}>
  //       <Button variant="danger">Anomalie</Button>
  //     </OverlayTrigger>
  //   ) : null;

  const checkValidHours = (hOuverture, hFermeture) =>
    (FormatInputTimeHHMMToDecimal(hOuverture) >=
      FormatInputTimeHHMMToDecimal(hFermeture) &&
      hOuverture !== -1 &&
      FormatInputTimeHHMMToDecimal(hFermeture) !== -1) ||
    hOuverture >= 24 ||
    hFermeture >= 24 ||
    hOuverture < -1 ||
    hFermeture < -1 ? (
      <OverlayTrigger trigger="click" placement="right" overlay={popover}>
        <Button variant="danger">Anomalie</Button>
      </OverlayTrigger>
    ) : null;

  if (props.idBib === "0" || props.idBib === null) {
    return null;
  } else {
    return (
      <div>
        <h3>{bibliotheque.name}</h3>
        <Form>
          <Form.Label>Réservations</Form.Label>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {bibliotheque.defaultResaDurationHours} h
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Durée par défaut de la réservation immédiate"
              aria-label="Durée par défaut de la réservation immédiate"
              onChange={(e) => {
                console.log(e.target.value);
                setDefaultResaDurationHours(e.target.value);
              }}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <Form.Label>Horaires d'ouverture</Form.Label>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourMonday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(openingHourMonday, bibliotheque.closingHourMonday)}
            <FormControl
              placeholder="Horaire d'ouverture le lundi"
              aria-label="Horaire d'ouverture le lundi"
              onChange={(e) => setOpeningHourMonday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourMonday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(bibliotheque.openingHourMonday, closingHourMonday)}
            <FormControl
              placeholder="Horaire de fermeture le lundi"
              aria-label="Horaire de fermeture le lundi"
              onChange={(e) => setClosingHourMonday(e.target.value)}
            />

            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourTuesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              openingHourTuesday,
              bibliotheque.closingHourTuesday
            )}
            <FormControl
              placeholder="Horaire d'ouverture le mardi"
              aria-label="Horaire d'ouverture le mardi"
              onChange={(e) => setOpeningHourTuesday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourTuesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              bibliotheque.openingHourTuesday,
              closingHourTuesday
            )}
            <FormControl
              placeholder="Horaire de fermeture le mardi"
              aria-label="Horaire de fermeture le mardi"
              onChange={(e) => setClosingHourTuesday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourWednesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              openingHourWednesday,
              bibliotheque.closingHourWednesday
            )}
            <FormControl
              placeholder="Horaire d'ouverture le mercredi"
              aria-label="Horaire d'ouverture le mercredi"
              onChange={(e) => setOpeningHourWednesday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourWednesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              bibliotheque.openingHourWednesday,
              closingHourWednesday
            )}
            <FormControl
              placeholder="Horaire de fermeture le mercredi"
              aria-label="Horaire de fermeture le mercredi"
              onChange={(e) => setClosingHourWednesday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourThursday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              openingHourThursday,
              bibliotheque.closingHourThursday
            )}
            <FormControl
              placeholder="Horaire d'ouverture le jeudi"
              aria-label="Horaire d'ouverture le jeudi"
              onChange={(e) => setOpeningHourThursday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourThursday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              bibliotheque.openingHourThursday,
              closingHourThursday
            )}
            <FormControl
              placeholder="Horaire de fermeture le jeudi"
              aria-label="Horaire de fermeture le jeudi"
              onChange={(e) => setClosingHourThursday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourFriday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(openingHourFriday, bibliotheque.closingHourFriday)}
            <FormControl
              placeholder="Horaire d'ouverture le vendredi"
              aria-label="Horaire d'ouverture le vendredi"
              onChange={(e) => setOpeningHourFriday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourFriday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(bibliotheque.openingHourFriday, closingHourFriday)}
            <FormControl
              placeholder="Horaire de fermeture le vendredi"
              aria-label="Horaire de fermeture le vendredi"
              onChange={(e) => setClosingHourFriday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourSaturday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              openingHourSaturday,
              bibliotheque.closingHourSaturday
            )}
            <FormControl
              placeholder="Horaire d'ouverture le samedi"
              aria-label="Horaire d'ouverture le samedi"
              onChange={(e) => setOpeningHourSaturday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourSaturday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(
              bibliotheque.openingHourSaturday,
              closingHourSaturday
            )}
            <FormControl
              placeholder="Horaire de fermeture le samedi"
              aria-label="Horaire de fermeture le samedi"
              onChange={(e) => setClosingHourSaturday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourSunday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(openingHourSunday, bibliotheque.closingHourSunday)}
            <FormControl
              placeholder="Horaire d'ouverture le dimanche"
              aria-label="Horaire d'ouverture le dimanche"
              onChange={(e) => setOpeningHourSunday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourSunday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            {checkValidHours(bibliotheque.openingHourSunday, closingHourSunday)}
            <FormControl
              placeholder="Horaire de fermeture le dimanche"
              aria-label="Horaire de fermeture le dimanche"
              onChange={(e) => setClosingHourSunday(e.target.value)}
            />
            <InputGroup.Append>
              <Button
                disabled={!canSubmit}
                onClick={onSubmit}
                variant="outline-secondary"
              >
                Modifier
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </div>
    );
  }
}

function DisplayTimeHHMM(decimalTime) {
  if (decimalTime) {
    return decimalToTimeStr(decimalTime);
  }
  return "fermé";
}

function FormatInputTimeHHMMToDecimal(strDecimalTime) {
  console.log(typeof strDecimalTime);
  if (typeof strDecimalTime === "number") {
    if (
      isFinite(strDecimalTime) &&
      strDecimalTime <= 23 &&
      strDecimalTime >= 0
    ) {
      console.log("decimal true");

      return strDecimalTime;
    }
  } else if (
    typeof strDecimalTime === "string" &&
    !strDecimalTime.includes(":")
  ) {
    const heure = Number.parseInt(strDecimalTime);
    if (!Number.isNaN(heure)) {
      if (isFinite(heure) && heure <= 23 && heure >= 0) {
        console.log("string true");

        return heure;
      }
    }
  }

  if (typeof strDecimalTime === "string" && strDecimalTime.includes(":")) {
    console.log("parse string");
    if (strDecimalTime === -1) {
      return -1;
    }
    console.log("split");
    console.log(typeof strDecimalTime);

    const input = strDecimalTime.split(":");
    const heures = parseInt(input[0]);
    const minutes = parseInt(input[1]);
    if (
      isFinite(heures) &&
      heures <= 23 &&
      heures >= 0 &&
      isFinite(minutes) &&
      minutes <= 59 &&
      minutes >= 0
    ) {
      return timeStrToDecimal(strDecimalTime);
    }
  }
  return -1;
}
