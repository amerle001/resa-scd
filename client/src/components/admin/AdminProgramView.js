import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

import { selectUser } from "./userSlice";
import AdminProgramResa from "./AdminProgramResa";
import AdminDashResaControl from "./AdminDashResaControl";
import UserSearch from "./UserSearch";

export default function AdminProgramView() {
  const userData = useSelector(selectUser);
  const history = useHistory();

  const [userSelectedFromSearch, setUserSelectedFromSearch] = useState(null);

  useEffect(() => {
    if (!userData) {
      setTimeout(() => {
        history.push("/");
      }, 2000);
    }
  }, []);

  return (
    <div>
      {userData ? (
        <div>
          <div className="admin-container">
            <div className="admin-sidebar">
              <AdminProgramResa
                userSelectedFromSearch={userSelectedFromSearch}
              />
            </div>
            <div className="admin-content">
              {/* <h2>Mes réservations</h2>
              <AdminDashResaControl /> */}
              <UserSearch
                onSelect={(utilisateur) =>
                  setUserSelectedFromSearch(utilisateur)
                }
              />
            </div>
          </div>
        </div>
      ) : (
        <h1>Connectez-vous pour accéder à cette page.</h1>
      )}
    </div>
  );
}
