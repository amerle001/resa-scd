import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  userBase: [],
  fetchStatus: "idle",
  deleteUserStatus: "idle",
  error: null,
};

export const fetchUserBase = createAsyncThunk(
  "userBase/fetchUserBase",
  async () => {
    const response = await axios.get("/users/userBase", {
      withCredentials: true,
    });
    return response.data;
  }
);

export const deleteUser = createAsyncThunk(
  "userBase/deleteUser",
  async (userId) => {
    const response = await axios.delete(`/users/userBase/${userId}`, {
      withCredentials: true,
    });
    console.log(response.data);
    return response.data.userDeleted._id;
  }
);

export const modifyPrivilegeLevel = createAsyncThunk(
  "userBase/modifyPrivilegeLevel",
  async (modifyPrivilegeData) => {
    const response = await axios.post("/users/userBase", modifyPrivilegeData, {
      withCredentials: true,
    });
    console.log(response.data);
    return response.data;
  }
);

export const userBaseSlice = createSlice({
  name: "userBase",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchUserBase.pending]: (state, action) => {
      state.fetchStatus = "loading";
    },
    [fetchUserBase.fulfilled]: (state, action) => {
      state.fetchStatus = "succeeded";
      state.userBase = action.payload;
    },
    [fetchUserBase.rejected]: (state, action) => {
      state.fetchStatus = "failed";
      state.error = action.error.message;
    },
    [deleteUser.pending]: (state, action) => {
      state.deleteUserStatus = "pending";
    },
    [deleteUser.fulfilled]: (state, action) => {
      state.userBase = state.userBase.filter(
        (user) => user._id !== action.payload
      );
      state.deleteUserStatus = "idle";
    },
    [deleteUser.rejected]: (state, action) => {
      state.deleteUserStatus = "failed";
    },
    [modifyPrivilegeLevel.fulfilled]: (state, action) => {
      state.userBase = state.userBase.filter(
        (user) => user._id !== action.payload._id
      );
      state.userBase.push(action.payload);
    },
  },
});

export default userBaseSlice.reducer;

// SELECTORS
export const selectUserBase = (state) => state.userBase.userBase;
