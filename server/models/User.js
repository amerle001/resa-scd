const mongoose = require("mongoose").set("debug", true);

// Création du schéma
// prenom, nom, email, password, isactive, privilegeLevel (superadmin, admin, user), register_date
const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  register_date: {
    type: Date,
    default: Date.now,
  },
  privilege_level: {
    type: String, // superadmin, admin, user
    default: "user",
  },
  is_active: {
    type: Boolean,
    default: false,
  },
  temporaire: {
    type: Boolean,
    default: false,
  },
  has_already_a_seat: {
    type: Boolean,
    default: false,
  },
  reservations: [
    {
      locationId: String,
      placeId: String,
      idNewResaFromPlace: String,
      locationName: String,
      placeNumber: Number,
      start: Date,
      // end: Date,       // plus de 7 items, ça ne passe pas dans MongoDB ???
      durationInMinutes: Number,
    },
  ],
});

// Création et export du modèle

module.exports = User = mongoose.model("user", UserSchema);
// module.exports = mongoose.model("User", UserSchema);
