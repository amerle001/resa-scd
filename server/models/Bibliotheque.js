const mongoose = require("mongoose").set("debug", true);
const Schema = mongoose.Schema;

// Creation du Schema Mongoose
const BibliothequeSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  contenance: {
    type: Number,
    required: true,
  },
  places: [
    {
      nom: String,
      numero: Number,
      startReservation: Date,
      endReservation: Date,
      durationInMinutes: Number,
      libre: Boolean,
      futureResa: [
        {
          nom: String,
          numero: Number,
          fStartReservation: Date,
          fEndReservation: Date,
          durationInMinutes: Number,
        },
      ],
    },
  ],
  defaultResaDurationHours: {
    type: Number,
    default: 1,
  },
  openingHourMonday: {
    type: Number,
    default: 8,
  },
  openingHourTuesday: {
    type: Number,
    default: 8,
  },
  openingHourWednesday: {
    type: Number,
    default: 8,
  },
  openingHourThursday: {
    type: Number,
    default: 8,
  },
  openingHourFriday: {
    type: Number,
    default: 8,
  },
  openingHourSaturday: {
    type: Number,
    default: 8,
  },
  openingHourSunday: {
    type: Number,
    default: null,
  },
  closingHourMonday: {
    type: Number,
    default: 18,
  },
  closingHourTuesday: {
    type: Number,
    default: 18,
  },
  closingHourWednesday: {
    type: Number,
    default: 18,
  },
  closingHourThursday: {
    type: Number,
    default: 18,
  },
  closingHourFriday: {
    type: Number,
    default: 18,
  },
  closingHourSaturday: {
    type: Number,
    default: 18,
  },
  closingHourSunday: {
    type: Number,
    default: null,
  },
});

module.exports = Bibliotheque = mongoose.model(
  "bibliotheque",
  BibliothequeSchema
);
