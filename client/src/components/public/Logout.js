import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import AlertTimeOut from "./AlertTimeOut";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  logoutUser,
  selectLoginStatus,
  selectLogoutStatus,
  selectUser,
} from "../admin/userSlice";

export default function Login() {
  const dispatch = useDispatch();
  const loginStatus = useSelector(selectLoginStatus);
  const logoutStatus = useSelector(selectLogoutStatus);
  const userData = useSelector(selectUser);
  const history = useHistory();

  const [logoutRequestStatus, setLogoutRequestStatus] = useState("idle");

  const canLogout =
    loginStatus === "LOGIN_SUCCESS" && logoutRequestStatus === "idle";

  const onLogout = async () => {
    if (canLogout) {
      try {
        setLogoutRequestStatus("pending");
        const resultAction = await dispatch(logoutUser());
        unwrapResult(resultAction);
        history.push("/");
      } catch (error) {
        console.log("échec de la déconnexion : ", error);
      } finally {
        setLogoutRequestStatus("idle");
      }
    }
  };

  const displayAlert = () => {
    switch (logoutStatus) {
      case "LOGOUT_SUCCESS":
        return <AlertTimeOut type="success" message="Vous êtes déconnecté." />;

      case "LOGOUT_FAILURE":
        return <AlertTimeOut type="danger" message="Echec de la déconnexion" />;

      default:
        return null;
    }
  };

  return (
    <div>
      {logoutRequestStatus === "idle" ? displayAlert() : null}

      {userData ? (
        <Button
          className="logout"
          variant="outline-light"
          onClick={onLogout}
          disabled={!canLogout}
        >
          Déconnexion
        </Button>
      ) : // <img
      //   className="logout"
      //   alt="logout"
      //   src="./logout.svg"
      //   width="32"
      //   height="32"
      //   onClick={onLogout}
      // />
      null}
    </div>
  );
}
