import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  selectScd,
  selectBibAffichee,
  afficherBibliotheque,
} from "../admin/scdSlice";
import Button from "react-bootstrap/Button";

export default function UserDashControl() {
  const dispatch = useDispatch();
  const scdRedux = useSelector(selectScd);
  const bibAffichee = useSelector(selectBibAffichee);

  return (
    <div>
      {scdRedux.map((bib) => (
        <div className="dashboard-row" key={bib._id}>
          <div className="dash-bib-name">{bib.name}</div>
          <div className="dashControl-buttons">
            <Button
              variant="outline-dark"
              size="sm"
              onClick={(e) => dispatch(afficherBibliotheque(bib._id))}
            >
              {bibAffichee
                ? bibAffichee._id === bib._id
                  ? "Réduire"
                  : "Afficher"
                : "Afficher"}
            </Button>
          </div>
        </div>
      ))}
    </div>
  );
}
