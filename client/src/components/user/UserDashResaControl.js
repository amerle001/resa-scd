import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { selectUser, getUser } from "../admin/userSlice";
import { selectScd, deleteResa } from "../admin/scdSlice";
import { formatDateForHumans } from "../admin/UserBase";
import Button from "react-bootstrap/Button";

export default function UserDashResaControl() {
  const dispatch = useDispatch();
  const userData = useSelector(selectUser);
  const scd = useSelector(selectScd);
  const programResaStatus = useSelector((state) => state.scd.programResaStatus);

  const [deleteRequestStatus, setDeleteRequestStatus] = useState("idle");
  const canDelete =
    userData && deleteRequestStatus === "idle" && programResaStatus === null;

  const onDelete = async (infoDeleteFResa) => {
    if (canDelete) {
      try {
        setDeleteRequestStatus("pending");
        const resultAction = await dispatch(deleteResa(infoDeleteFResa));
        unwrapResult(resultAction);
        console.log(resultAction);

        await dispatch(getUser());
      } catch (error) {
        console.log(error);
      } finally {
        setDeleteRequestStatus("idle");
      }
    }
  };

  useEffect(() => {
    const resultAction = dispatch(getUser());
    unwrapResult(resultAction);
  }, [programResaStatus]);

  return (
    <div>
      <p>{userData.username}</p>

      <div className="wrapper">
        {userData.reservations.map((resa) => (
          <div className="libre" key={resa._id}>
            <p>
              <strong>Lieu : </strong> {resa.locationName}
            </p>
            <p>
              <strong>N° de place : </strong> {resa.placeNumber}
            </p>
            <p>
              <strong>Date : </strong> {formatDateForHumans(resa.start)}
            </p>
            <p>
              <strong>Heure : </strong> {new Date(resa.start).getHours()}:
              {new Date(resa.start).getMinutes()}
            </p>
            <p>
              <strong>Durée (mn) : </strong> {resa.durationInMinutes}
            </p>
            <Button
              variant="outline-dark"
              size="sm"
              block
              onClick={(e) =>
                onDelete({
                  nomReservant: userData.username,
                  idBib: resa.locationId,
                  idPlace: resa.placeId,
                  fResaId: resa.idNewResaFromPlace,
                  userResaId: resa._id,
                })
              }
              disabled={!canDelete}
            >
              Libérer place
            </Button>
          </div>
        ))}
      </div>
    </div>
  );
}
