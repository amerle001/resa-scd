const express = require("express");
const router = express.Router();

// Bibliotheques Model
const Bibliotheque = require("../../models/Bibliotheque");
const User = require("../../models/User");

// @route   GET api/bibliotheques
// @desc    Get toutes les bibs du SCD. Classement descendant par nombre de places.
// @access  PUBLIC
router.get("/", (req, res) => {
  Bibliotheque.find()
    .sort({ contenance: -1 })
    .then((bibliotheques) => res.json(bibliotheques))
    .catch((err) =>
      res.status(404).json({ get_bibliotheques_success: false, message: err })
    );
});

// @route   POST api/bibliotheques
// @desc    Créer une bibliotheque vide avec son nombre nominal de places
// @access  PUBLIC
router.post("/", (req, res) => {
  const salleVideInit = [];
  for (let i = 0; i < req.body.contenance; i++) {
    salleVideInit.push({
      nom: "",
      numero: i + 1,
      startReservation: null,
      endReservation: null,
      durationInMinutes: null,
      libre: true,
    });
  }

  const newBibliotheque = new Bibliotheque({
    name: req.body.name,
    contenance: req.body.contenance,
    places: salleVideInit,
  });
  newBibliotheque
    .save()
    .then((bibliotheque) => res.json(bibliotheque))
    .catch((err) =>
      res.status(404).json({ creation_success: false, message: err })
    );
});

// @route   DELETE api/bibliotheques
// @desc    Supprimer une bib du SCD
// @access  PUBLIC
// :id === placeholder for the id we'll pass
router.delete("/:id", async (req, res) => {
  await Bibliotheque.findById(req.params.id).then((bibliotheque) =>
    bibliotheque
      .remove()
      .then(() =>
        res.json({ delete_success: true, bibliothequeDeleted: bibliotheque })
      )
      .catch((err) =>
        res.status(404).json({ delete_success: false, message: err })
      )
  );
  await nettoieResaDansBibSupprimee(req.params.id);
});

const nettoieResaDansBibSupprimee = async (bibId) => {
  const users = await User.find();
  console.log(users);

  for (let user of await User.find()) {
    // array neuf qui filtre les resa rattachées à la bib supprimée
    // puis update en bloc si cet array est < à array des reservations
    const tempReservations = user.reservations.filter(
      (resa) => resa.locationId !== bibId
    );
    if (tempReservations.length < user.reservations.length) {
      await User.updateOne(
        {
          _id: user._id,
        },
        {
          reservations: [...tempReservations],
        }
      )
        .catch((err) => console.log(err))
        .then(() => {
          console.log(
            `fResa User nettoyée suite à suppression de bibliothèque.`
          );
        });
    }
  }
};

module.exports = router;
