import React, { useEffect } from "react";
import Login from "./Login";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectUser } from "../admin/userSlice";

export default function LoginView() {
  let userData = useSelector(selectUser);
  const history = useHistory();

  useEffect(() => {
    if (userData) {
      setTimeout(() => {
        history.push("/");
      }, 1300); // + 300 par rapport à AlertTimeOut car sinon : Can't perform a React state update on an unmounted component
    }
  }, [userData]);

  return (
    <div>
      <div className="loginViewParent">
        <div className="loginViewChild">
          <Login />
        </div>
      </div>
    </div>
  );
}
