const express = require("express");
const router = express.Router();

const Bibliotheque = require("../../models/Bibliotheque");
const User = require("../../models/User");

// @route   POST api/deleteResa
// @desc    supprimer une réservation programmée
// @access  PUBLIC

router.post("/", (req, res) => {
  (async () => {
    const { nomReservant, idBib, idPlace, fResaId, userResaId } = req.body;
    console.log(nomReservant);
    console.log(idBib);
    console.log(idPlace);
    console.log(fResaId);
    console.log(userResaId);
    console.log(userResaId);

    // si ce n'est pas une resa immédiate, il faut supprimer la fResa
    // si c'est une resa immédiate, par définition elle n'y sera pas
    if (fResaId !== "resaImmediate") {
      const bib = await Bibliotheque.findById(idBib);
      const place = await bib.places.id(idPlace);
      await place.futureResa.pull({ _id: fResaId });
      await bib
        .save()
        .catch((err) => console.log(err))
        .then(() => {
          console.log("ok resa bib supprimée.");
        });

      const user = await User.findOne({ username: nomReservant });
      await user.reservations.pull({ _id: userResaId });
      await user
        .save()
        .catch((err) => console.log(err))
        .then(() => {
          console.log("Resa has been removed from user account.");
        });

      const updatedBib = await Bibliotheque.findById(idBib);
      await res.json(updatedBib);
    } else {
      // ce n'est pas une resa immédiate, mais elle est active
      // il faut la supprimer en racine de place de bibliothèque et non en array FResa

      // *****ATTENTION******
      // logique dupliquée liberePlace
      // *****ATTENTION******

      // collation des infos depuis la requête frontend
      console.log("________DELETERESA________");

      // const { nomReservant, idPlace, idBib, startReservation } = req.body;
      // console.log(idPlace);
      // console.log(idBib);

      // le test sur placeEnQuestion[0] est une sécurité
      // si jamais la requête frontend est faite sur place qui est déjà libérée par
      // la routine de libération, on se contente de renvoyer la bib telle quelle
      // sinon, le vrai processus de libération s'enclenche

      const bibEnQuestion = await Bibliotheque.findById(idBib);
      const arrayPlaces = bibEnQuestion.places;
      const placeEnQuestion = arrayPlaces.filter(
        (place) => place._id == idPlace
      );

      if (placeEnQuestion[0].libre) {
        res.json(bibEnQuestion);
      } else {
        // Création d'une copie de l'état à jour à partir de l'ancien état
        const currentState = await Bibliotheque.findById(idBib).select(
          "contenance"
        );
        const ancienneContenance = currentState.contenance;
        const nouvelleContenance = ancienneContenance + 1;

        // MaJ de la db : contenance et réinitialisation de la place libérée
        await Bibliotheque.updateOne(
          {
            _id: idBib,
            "places._id": idPlace,
          },
          {
            contenance: nouvelleContenance,
            "places.$.nom": "",
            "places.$.startReservation": null,
            "places.$.endReservation": null,
            "places.$.durationInMinutes": null,
            "places.$.libre": true,
          }
        ).catch((err) => console.log(err));

        await User.findOneAndUpdate(
          { username: nomReservant },
          { has_already_a_seat: false },
          () => console.log("has_already_a_seat: updated")
        );

        const user = await User.findOne({ username: nomReservant });
        // const userReservations = await user.reservations;
        // attention : pour faire matcher la resa à supprimer
        // il faut à la fois vérifier que l'idPlace et le timeStamp correspondent
        // car l'user peut avoir 2 resas sur la même place, à 2 moments différents
        // donc vérifier uniquement idPlace = comportement indéfini
        // const userReservationToDelete = await user.reservations.filter(
        //   (resa) =>
        //     resa.placeId === idPlace &&
        //     new Date(resa.start).getTime() ===
        //       new Date(startReservation).getTime()
        // );
        // const userResaId = await userReservationToDelete[0]._id;

        console.log(`userResaId : ${userResaId}`);

        await user.reservations.pull({ _id: userResaId });
        await user
          .save()
          .catch((err) => console.log(err))
          .then(() => {
            console.log("Resa has been removed from user account.");
          });

        // Renvoi de la bibliothèque complète pour update Redux
        const response = await Bibliotheque.findById(idBib);
        res.json(response);
      }
    }
  })();
});

module.exports = router;
