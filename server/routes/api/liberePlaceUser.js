const express = require("express");
const router = express.Router();

// Bibliotheque et User Models
const Bibliotheque = require("../../models/Bibliotheque");
const User = require("../../models/User");

// @route   POST api/liberePlaceUser
// @desc    invoque middleware de Mongoose : Model.updateOne() (findByIdAndUpdate() ne fonctionne pas sur id nichés)
// @access  PUBLIC
router.post("/", (req, res) => {
  (async () => {
    // collation des infos depuis la requête frontend
    console.log("________LIBEREPLACEUSER________");

    console.log(req.body);

    // const { idPlace, idBib } = req.body;
    const { nomLiberant, idBib } = req.body;
    console.log(nomLiberant);
    console.log(idBib);

    // Création d'une copie de l'état à jour à partir de l'ancien état
    const currentState = await Bibliotheque.findById(idBib).select(
      "contenance"
    );
    const ancienneContenance = currentState.contenance;
    const nouvelleContenance = ancienneContenance + 1;

    // MaJ de la db : contenance et réinitialisation de la place libérée
    await Bibliotheque.updateOne(
      {
        _id: idBib,
        "places.nom": nomLiberant,
      },
      {
        contenance: nouvelleContenance,
        "places.$.nom": "",
        "places.$.startReservation": null,
        "places.$.endReservation": null,
        "places.$.durationInMinutes": null,
        "places.$.libre": true,
      }
    ).catch((err) => console.log(err));
    // .then(res.json({ "place libérée": idPlace }));

    // Update document usager : has_already_a_seat = false

    await User.findOneAndUpdate(
      { username: nomLiberant },
      { has_already_a_seat: false },
      () => console.log("has_already_a_seat: updated")
    )
      .clone()
      .catch((err) => console.log(err));

    const user = await User.findOne({ username: nomLiberant });
    // const resa = await user.reservations.filter(
    //   (res) =>
    //     res.start.getTime() + res.durationInMinutes * 60 * 1000 > Date.now()
    // );
    const resa = await user.reservations.filter(
      (res) => res.idNewResaFromPlace === "resaImmediate"
    );

    await user.reservations.pull({ _id: resa[0]._id });
    await user
      .save()
      .catch((err) => console.log(err))
      .then(() => {
        console.log("Ok user a libéré sa resa.");
      });

    // Renvoi de la bibliothèque complète pour update Redux
    const response = await Bibliotheque.findById(idBib);
    res.json(response);
  })();
});

module.exports = router;
