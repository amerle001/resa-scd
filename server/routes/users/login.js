const express = require("express");
const passport = require("passport");
const User = require("../../models/User");
require("../../config/passportConfig")(passport);
const router = express.Router();

// @route     POST /users/login
// @desc      login user
// @access    public

router.post("/", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) throw err;
    if (!user) res.send("LOGIN_FAILURE");
    else {
      req.logIn(user, (err) => {
        if (err) throw err;
        res.send({
          username: req.user.username,
          firstName: req.user.firstName,
          lastName: req.user.lastName,
          id: req.user.id,
          privilegeLevel: req.user.privilege_level,
          has_already_a_seat: req.user.has_already_a_seat,
          reservations: req.user.reservations,
        });
      });
    }
  })(req, res, next);
});

module.exports = router;
