import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

// props : action, actionParameter, canTrigger, buttonStyle, buttonContent, modalHeadingContent, modalBodyContent
export default function DisplayModal(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button
        type="button"
        size="sm"
        variant={props.buttonStyle}
        onClick={handleShow}
      >
        {props.buttonContent}
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{props.modalHeadingContent}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.modalBodyContent}</Modal.Body>
        <Modal.Footer>
          <Button size="sm" variant="secondary" onClick={handleClose}>
            Revenir
          </Button>
          <Button
            size="sm"
            variant={props.buttonStyle}
            onClick={() => {
              props.action(props.actionParameter);
              handleClose();
            }}
            disabled={props.canTrigger}
          >
            {props.buttonContent}
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
