import React, { Component } from "react";
import Salle from "../admin/Salle";
import Dashboard from "../admin/Dashboard";
import axios from "axios";

export class Bibliotheques extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scd: [],
      err: null,
      isLoading: false,
      nameNewBib: "",
      contenanceNewBib: 0,
      bibAfficheeId: null,
    };

    this.reservePlace = this.reservePlace.bind(this);
    this.liberePlace = this.liberePlace.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.delBib = this.delBib.bind(this);
    this.handleNewBibSubmit = this.handleNewBibSubmit.bind(this);
    this.handleNameNewBibChange = this.handleNameNewBibChange.bind(this);
    this.handleNbrePlaceNewBibChange = this.handleNbrePlaceNewBibChange.bind(
      this
    );
    this.afficherBib = this.afficherBib.bind(this);
    // this.setUserData = this.setUserData.bind(this);
  }

  async reservePlace(infosResa) {
    const sendReservePlaceRequest = async (infosResa) => {
      try {
        await axios
          .post("/api/reservePlace", infosResa)
          .then((res) =>
            console.log("infos resa : " + JSON.stringify(res.data))
          );
      } catch (error) {
        console.log(error);
      }
    };
    console.log(infosResa);
    await sendReservePlaceRequest(infosResa);

    await this.fetchData();
  }

  async liberePlace(idPlace, idBib) {
    const infosPlaceLiberee = {
      idPlace,
      idBib,
    };
    const sendLiberePlaceRequest = async (infosPlaceLiberee) => {
      try {
        await axios
          .post("/api/liberePlace", infosPlaceLiberee)
          .then((res) =>
            console.log("infos place libérée : " + JSON.stringify(res.data))
          );
      } catch (error) {
        console.log(error);
      }
    };

    await sendLiberePlaceRequest(infosPlaceLiberee);
    await this.fetchData();
  }

  async fetchData() {
    await axios
      .get("/api/bibliotheques")
      .then((res) => this.setState({ scd: res.data, isLoading: false }))
      .catch((error) => this.setState({ err: error, isLoading: false }));
  }

  async delBib(id) {
    await axios.delete(`/api/bibliotheques/${id}`).then((res) => {
      this.setState({ bibAfficheeId: null });
      console.log(res);
      this.fetchData();
    });
  }

  handleNameNewBibChange(e) {
    this.setState({ nameNewBib: e.target.value });
  }

  handleNbrePlaceNewBibChange(e) {
    this.setState({ contenanceNewBib: e.target.value });
  }

  handleNewBibSubmit = () => {
    // e.preventDefault();
    const newBibliotheque = {
      name: this.state.nameNewBib,
      contenance: this.state.contenanceNewBib,
    };
    axios
      .post("/api/bibliotheques", newBibliotheque)
      .then((res) => console.log("Nouvelle bibliothèque créée : " + res.data))
      .catch((err) => console.log(err.message));

    this.fetchData();
    this.setState({ nameNewBib: "", contenanceNewBib: 0 });
  };

  afficherBib(bibId) {
    this.setState({ bibAfficheeId: this.state.bibAfficheeId ? null : bibId });
  }

  async componentDidMount() {
    this.setState({ isLoading: true });
    this.fetchData();
  }

  render() {
    const { scd, err, isLoading, bibAfficheeId } = this.state;
    let site = "";
    let bibAfficheeContenance = 0;
    let places = [];

    const temp = scd.filter((bib) => bib._id === bibAfficheeId);
    console.log(temp);

    const bibAffichee = temp[0];

    if (this.state.bibAfficheeId !== null) {
      site = bibAffichee.name;
      bibAfficheeContenance = bibAffichee.contenance;
      places = bibAffichee.places;
      console.log("places");
      console.log(places);
    }

    if (err) {
      return <div>{err.message}</div>;
    }
    if (isLoading) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Dashboard
          // DashCreateBib component props
          name={this.state.nameNewBib}
          contenance={this.state.contenanceNewBib}
          handleNameNewBibChange={this.handleNameNewBibChange}
          handleNbrePlaceNewBibChange={this.handleNbrePlaceNewBibChange}
          handleNewBibSubmit={this.handleNewBibSubmit}
          // DashControler component props
          scd={this.state.scd}
          delBib={this.delBib}
          afficherBib={this.afficherBib}
          isDisplayed={bibAfficheeId}
        />

        {this.state.bibAfficheeId === null ? (
          <p>Veuillez sélectionner une bibliothèque à afficher.</p>
        ) : (
          <Salle
            site={site}
            idBib={bibAfficheeId}
            contenance={bibAfficheeContenance}
            reservePlace={this.reservePlace}
            liberePlace={this.liberePlace}
            etatSalle={places}
          />
        )}
      </div>
    );
  }
}

export default Bibliotheques;
