import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Link } from "react-router-dom";
import Logout from "./Logout";
import { useSelector, useDispatch } from "react-redux";
import { selectUser } from "../admin/userSlice";

const bonDropdown = (privilegeLevel) => {
  if (privilegeLevel === "admin" || privilegeLevel === "superadmin") {
    return MenuItems(
      "Menu",
      { route1: "/admin", description1: "Tableau de bord" },
      { route2: "/userBase", description2: "Utilisateurs" }
    );
  } else if (privilegeLevel === "user") {
    return MenuItems(
      "Réserver",
      { route1: "/userView", description1: "Pour maintenant" },
      { route2: "/userProgramView", description2: "Pour plus tard" }
    );
  } else {
    return MenuItems(
      "Menu",
      { route1: "/login", description1: "Se connecter" },
      { route2: "/register", description2: "S'inscrire" }
    );
  }
};

const MenuItems = (titre, item1, item2) => {
  const { route1, description1 } = item1;
  const { route2, description2 } = item2;
  return (
    <NavDropdown title={titre} id="basic-nav-dropdown">
      <NavDropdown.Item>
        <Link className="navbar-item" to={route1}>
          {description1}
        </Link>
      </NavDropdown.Item>
      <NavDropdown.Item>
        <Link className="navbar-item" to={route2}>
          {description2}
        </Link>
      </NavDropdown.Item>
    </NavDropdown>
  );
};

export default function UINavbar() {
  const userData = useSelector(selectUser);

  return (
    <div>
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand>
          <Link to="/">
            <img
              alt="resa-scd"
              src="./logo5.png"
              width="39"
              height="30"
              className="d-inline-block align-top"
            />
          </Link>{" "}
          <Link to="/" className="brand-name">
            Resa SCD
          </Link>
        </Navbar.Brand>
        <Nav className="mr-auto">
          {bonDropdown(userData ? userData.privilegeLevel : null)}
        </Nav>
        <Logout />
      </Navbar>
    </div>
  );
}
