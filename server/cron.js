const Bibliotheque = require("./models/Bibliotheque");
const User = require("./models/User");
const ResaLog = require("./models/ResaLog");

const cron = require("node-cron");

// reservationRoutine : regarde dans toutes les places de toutes les bib
// si elles ont un array de future resa, alors toutes sont examinées
// si l'une d'entre elle entre en vigueur maintenant
// elle est inscrite dans la place à la racine
// et le user.has_already_a_seat passe a true
// la réservation est loggée
const reservationRoutine = async () => {
  for (let bibliotheque of await Bibliotheque.find()) {
    let contenance = bibliotheque.contenance;
    for (let place of bibliotheque.places) {
      for (let fResa of place.futureResa) {
        // optimiser en mettant Date.now dans une variable
        if (
          Date.now() >= fResa.fStartReservation &&
          Date.now() <= fResa.fEndReservation
        ) {
          // update place resa actuelle
          --contenance;
          await Bibliotheque.updateOne(
            {
              _id: bibliotheque._id,
              "places._id": place._id,
            },
            {
              contenance: contenance,
              "places.$.nom": fResa.nom,
              "places.$.startReservation": fResa.fStartReservation,
              "places.$.endReservation": fResa.fEndReservation,
              "places.$.durationInMinutes": fResa.durationInMinutes,
              "places.$.libre": false,
            }
          )
            .catch((err) => console.log(err))
            .then(() => {
              console.log(
                "Cron : ok resa basculée de fResa vers racine place."
              );
            });

          // then delete fResa dans la place
          const tempBib = await Bibliotheque.findById(bibliotheque._id);
          const tempPlace = await tempBib.places.id(place._id);
          await tempPlace.futureResa.pull({ _id: fResa._id });
          await tempBib
            .save()
            .catch((err) => console.log(err))
            .then(() => {
              console.log("Cron : ok fresa supprimée.");
            });

          // then update User
          await User.findOneAndUpdate(
            { username: fResa.nom },
            { has_already_a_seat: true }
          );

          // finally log resa
          const userLogged = await User.findOne({ username: fResa.nom });

          const logEntry = new ResaLog({
            firstName: await userLogged.firstName,
            lastName: await userLogged.lastName,
            username: await userLogged.username,
            location: await bibliotheque.name,
            placeNumber: await tempPlace.numero,
            startPresence: await fResa.fStartReservation,
            endPresence: await fResa.fEndReservation,
            stayDurationInMinutes: await fResa.durationInMinutes,
          });

          await logEntry
            .save()
            .then((entry) => console.log(entry))
            .catch((err) => console.log(err));
        }
      }
    }
  }
  console.log("___CRON_RESERVATION_ROUTINE___");
};

const liberationRoutine = async () => {
  // liberation des places dont la réservation a expiré

  for (let bibliotheque of await Bibliotheque.find()) {
    let contenance = bibliotheque.contenance;
    for (let place of bibliotheque.places) {
      if (!place.libre) {
        if (Date.now() >= place.endReservation) {
          console.log("test réussi");
          ++contenance;
          console.log("contenance :" + contenance);

          await Bibliotheque.updateOne(
            {
              _id: bibliotheque._id,
              "places._id": place._id,
            },
            {
              // contenance: bibliotheque.contenance + 1,
              contenance: contenance,
              "places.$.nom": "",
              "places.$.startReservation": null,
              "places.$.endReservation": null,
              "places.$.durationInMinutes": null,
              "places.$.libre": true,
            }
          );
          await User.findOneAndUpdate(
            { username: place.nom },
            { has_already_a_seat: false }
          )
            .catch((err) => console.log(err))
            .then(() => {
              console.log("Cron : ok user has_already_a_seat: false.");
            });
        }
      }
    }
  }

  console.log("___CRON_LIBERATION_ROUTINE___");
};

const userFResaLiberationRoutine = async () => {
  for (let user of await User.find()) {
    for (let fResa of user.reservations) {
      if (
        Date.now() >
        fResa.start.getTime() + fResa.durationInMinutes * 60 * 1000
      ) {
        await user.reservations.pull({ _id: fResa._id });
        await user
          .save()
          .catch((err) => console.log(err))
          .then(() => {
            console.log("Cron : ok fresa User périmée a été supprimée.");
          });
      }
    }
  }
  console.log("___CRON_USER_FRESA_LIBERATION_ROUTINE___");
};

// transformer en requête manuelle
// déclenche la libération de vieilles fResa qui n'auraient pas été collectées
// par reservationRoutine
const fResaLiberationManual = async () => {
  for (let bibliotheque of await Bibliotheque.find()) {
    for (let place of bibliotheque.places) {
      if (place.futureResa.length) {
        for (let fResa of place.futureResa) {
          // console.log(fResa);
          if (Date.now() > fResa.fEndReservation) {
            const tempBib = await Bibliotheque.findById(bibliotheque._id);
            const tempPlace = await tempBib.places.id(place._id);
            await tempPlace.futureResa.pull({ _id: fResa._id });
            await tempBib
              .save()
              .catch((err) => console.log(err))
              .then(() => {
                console.log("Suppression manuelle fResa réussie.");
              });
          }
        }
      }
    }
  }
  console.log("___USER_FRESA_LIBERATION_MANUAL___");
};

// détruit tous les comptes temporaires
const temporaryAccountCollector = async () => {
  for (let user of await User.find()) {
    if (
      user.temporaire &&
      user.register_date.getTime() + 86400000 < Date.now()
    ) {
      User.findByIdAndDelete(user._id).exec();
      console.log(`expired temp account ${user.lastName} deleted`);
    }
  }
  console.log("___CRON_TEMP_ACCOUNT_COLLECTOR___");
};

const reservationWatcher = async () => {
  await reservationRoutine();
  await liberationRoutine();
  await userFResaLiberationRoutine();
  await fResaLiberationManual();
  await temporaryAccountCollector();
};

cron.schedule("* * * * *", reservationWatcher);
// cron.schedule("* * * * *", reservationWatcher);

// module.exports = cron;
