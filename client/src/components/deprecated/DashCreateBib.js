import React from "react";

class CreateBib extends React.Component {
  render() {
    return (
      <div>
        <h1>Créer une nouvelle bibliothèque</h1>
        <form onSubmit={(e) => this.props.handleNewBibSubmit(e)}>
          <label>
            Entrez le nom de la bibliothèque à créer :
            <input
              placeholder="nom BU"
              type="String"
              value={this.props.name}
              onChange={this.props.handleNameNewBibChange}
            />
          </label>
          <label>
            Entrez son nombre de places assises :
            <input
              type="Number"
              value={this.props.contenance}
              onChange={this.props.handleNbrePlaceNewBibChange}
            />
          </label>
          <button>Valider</button>
        </form>
      </div>
    );
  }
}

export default CreateBib;
