// configuration, variables d'environnement
require("dotenv").config({ path: "./server/config/.env" });
// const test = require("./config/.env");
// ----------------- Imports ---------------------
const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const session = require("express-session");
require("./config/passportConfig")(passport); // strategie locale passée dans une instance unique de passport
const cron = require("./cron");
const app = express();

// ----------------- Middlewares ---------------------
// BodyParser est inclus dans Express
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: `${process.env.EXPRESS_SESSION_SECRET}`,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(cookieParser(process.env.EXPRESS_SESSION_SECRET));
app.use(passport.initialize()); // middleware pour initialiser passport
app.use(passport.session()); // middleware pour initialiser session de passport

// ----------------- MongoDB ---------------------
console.log("mongo creds");

const db = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.oboi9.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Atlas connecté"))
  .catch((err) => console.log(err));

// ----------------- routes ---------------------
app.use("/api/bibliotheques", require("./routes/api/bibliotheques"));
app.use("/api/reservePlace", require("./routes/api/reservePlace"));
app.use("/api/liberePlace", require("./routes/api/liberePlace"));
app.use("/api/liberePlaceUser", require("./routes/api/liberePlaceUser"));
app.use("/users/login", require("./routes/users/login"));
app.use("/users/logout", require("./routes/users/logout"));
app.use("/users/register", require("./routes/users/register"));
app.use("/users/getUser", require("./routes/users/getUser"));
app.use("/users/userBase", require("./routes/users/userBase"));
app.use("/api/programResa", require("./routes/api/programResa"));
app.use("/api/configure", require("./routes/api/configure"));

app.get("/", (req, res) => {
  res.send("Agur Xiberoa !");
});

app.get("/error", (req, res) => {
  res.send("Oops, quelque chose n'a pas bien fonctionné !");
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Serveur écoute sur le port ${port}`);
});
