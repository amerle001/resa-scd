const User = require("../models/User");
const bcrypt = require("bcryptjs");
const localStrategy = require("passport-local").Strategy;

module.exports = function (passport) {
  passport.use(
    new localStrategy((username, password, done) => {
      User.findOne({ username: username }, (err, user) => {
        if (err) throw err;
        if (!user) return done(null, false, { message: "Login incorrect." });
        bcrypt.compare(password, user.password, (err, result) => {
          if (err) throw err;
          if (result === true) {
            return done(null, user);
          } else {
            console.log("passwords do not match");
            return done(null, false);
          }
        });
      });
    })
  );

  // stores a cookie in the browser with user id
  passport.serializeUser((user, cb) => {
    cb(null, user.id);
  });

  // takes that cookie, returns user from it
  passport.deserializeUser((id, cb) => {
    User.findOne({ _id: id }, (err, user) => {
      const userInformation = {
        username: user.username, // Sécurité : on ne retourne que l'information filtrée au client. Pas le password hash.
        firstName: user.firstName,
        lastName: user.lastName,
        id: user._id,
        privilegeLevel: user.privilege_level,
        has_already_a_seat: user.has_already_a_seat,
        reservations: user.reservations,
      };
      cb(err, userInformation);
    });
  });
};
