import React from "react";
import { useSelector } from "react-redux";
import { selectUser } from "../admin/userSlice";
import Accueil from "../public/Accueil";
import UINavbar from "../public/UINavbar";

export default function CheckAuth() {
  const userData = useSelector(selectUser);

  return (
    <div>
      <UINavbar />
      <Accueil userData={userData} />
    </div>
  );
}
