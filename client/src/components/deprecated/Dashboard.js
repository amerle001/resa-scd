import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Collapse from "react-bootstrap/Collapse";
import DashControl from "./DashControl";
import DashCreateBib from "./DashCreateBib";

const Example = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Button
        variant="primary"
        onClick={toggle}
        aria-controls="tdb"
        aria-expanded={isOpen}
      >
        Tableau de bord
      </Button>

      <Collapse in={isOpen}>
        <div id="tdb">
          <DashCreateBib />
          <DashControl />
        </div>
      </Collapse>
    </div>
  );
};

export default Example;
