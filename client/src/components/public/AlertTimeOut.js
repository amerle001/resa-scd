import React, { useState, useEffect } from "react";
import Alert from "react-bootstrap/Alert";

export default function AlertTimeOut(props) {
  const [visible, setVisible] = useState(true);

  const timeOutMessage = async () => {
    window.setTimeout(() => {
      setVisible(false);
    }, 1000);
  };

  useEffect(() => {
    timeOutMessage();
  }, []);

  return (
    <div>
      {visible ? <Alert variant={props.type}>{props.message}</Alert> : null}
    </div>
  );
}
