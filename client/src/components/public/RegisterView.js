import React, { useEffect } from "react";
import Register from "./Register";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectRegisterStatus } from "../admin/userSlice";

export default function RegisterView() {
  const registerSuccess = useSelector(selectRegisterStatus);
  const history = useHistory();

  useEffect(() => {
    if (registerSuccess === "REGISTER_SUCCESS") {
      setTimeout(() => {
        history.push("/");
      }, 1300); // + 300 par rapport à AlertTimeOut car sinon : "Can't perform a React state update on an unmounted component"
    }
  }, [registerSuccess]);

  return (
    <div>
      <div className="loginViewParent">
        <div className="loginViewChild">
          <Register />{" "}
        </div>
      </div>
    </div>
  );
}
