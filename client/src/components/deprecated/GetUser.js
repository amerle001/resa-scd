import React from "react";

export default function GetUser(props) {
  return (
    <div>
      <h2>Get user</h2>
      <button onClick={props.getUser}>Get user</button>
    </div>
  );
}
