import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";

import { decimalToTimeStr, timeStrToDecimal } from "pretty-time-decimal";

import { selectScd, fetchScd } from "./scdSlice";

import { selectUser } from "./userSlice";
import ConfOpeningHours from "./ConfOpeningHours";

function DisplayTimeHHMM(decimalTime) {
  if (decimalTime) {
    return decimalToTimeStr(decimalTime);
  }
  return "fermé";
}

function FormatInputTimeHHMMToDecimal(strDecimalTime) {
  if (strDecimalTime) {
    const input = strDecimalTime.split(":");
    const heures = parseInt(input[0]);
    const minutes = parseInt(input[1]);
    if (
      heures.isFinite() &&
      heures <= 23 &&
      heures >= 0 &&
      minutes.isFinite() &&
      minutes <= 59 &&
      minutes >= 0
    ) {
      return timeStrToDecimal(strDecimalTime);
    }
  }
  return -1;
}

export default function Configuration() {
  const userData = useSelector(selectUser);
  const scd = useSelector(selectScd);
  const history = useHistory();
  const dispatch = useDispatch();
  // const scdFetchStatus = useSelector((state) => state.scd.status);
  // const bibAffichee = useSelector(selectBibAffichee);

  const [lieu, setLieu] = useState(null);

  useEffect(() => {
    dispatch(fetchScd());
  }, []);

  return (
    <div>
      {scd ? (
        <div>
          <h1>Configuration</h1>

          <Form>
            <Form.Group>
              <Form.Label>Choisissez votre bibliothèque</Form.Label>
              <Form.Control
                as="select"
                className="mr-sm-2"
                id="inlineFormCustomSelect"
                custom
                onChange={(e) => setLieu(e.target.value)}
              >
                <option value="0">Bibliothèque</option>
                {scd.map((bib) => (
                  <option key={bib._id} value={bib._id}>
                    {bib.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          </Form>
          {/* {DisplayWeekOpeningHours(scd, lieu)} */}
          <ConfOpeningHours scd={scd} idBib={lieu} />
        </div>
      ) : null}
    </div>
  );
}

function DisplayWeekOpeningHours(scd, idBib) {
  if (idBib === "0" || idBib === null) {
    console.log(idBib);

    return null;
  } else {
    const bibliothequeTemp = scd.filter((bib) => bib._id === idBib);
    const bibliotheque = bibliothequeTemp[0];
    console.log(bibliothequeTemp);
    console.log(bibliotheque);

    return (
      <div>
        <h3>{bibliotheque.name}</h3>
        <Form>
          <Form.Label>Réservations</Form.Label>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {bibliotheque.defaultResaDurationHours} h
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Durée par défaut de la réservation immédiate"
              aria-label="Durée par défaut de la réservation immédiate"
              onChange={(e) => console.log(e.target.value)}
            />
            <InputGroup.Append>
              <Button variant="outline-secondary">Button</Button>
            </InputGroup.Append>
          </InputGroup>

          <Form.Label>Horaires d'ouverture</Form.Label>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourMonday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le lundi"
              aria-label="Horaire d'ouverture le lundi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourMonday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le lundi"
              aria-label="Horaire de fermeture le lundi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourTuesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le mardi"
              aria-label="Horaire d'ouverture le mardi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourTuesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le mardi"
              aria-label="Horaire de fermeture le mardi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourWednesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le mercredi"
              aria-label="Horaire d'ouverture le mercredi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourWednesday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le mercredi"
              aria-label="Horaire de fermeture le mercredi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourThursday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le jeudi"
              aria-label="Horaire d'ouverture le jeudi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourThursday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le jeudi"
              aria-label="Horaire de fermeture le jeudi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourFriday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le vendredi"
              aria-label="Horaire d'ouverture le vendredi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourFriday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le vendredi"
              aria-label="Horaire de fermeture le vendredi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourSaturday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le samedi"
              aria-label="Horaire d'ouverture le samedi"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourSaturday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le samedi"
              aria-label="Horaire de fermeture le samedi"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.openingHourSunday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire d'ouverture le dimanche"
              aria-label="Horaire d'ouverture le dimanche"
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>
                {DisplayTimeHHMM(bibliotheque.closingHourSunday)}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Horaire de fermeture le dimanche"
              aria-label="Horaire de fermeture le dimanche"
            />
          </InputGroup>
        </Form>
      </div>
    );
  }
}
