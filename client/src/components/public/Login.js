import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import AlertTimeOut from "./AlertTimeOut";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { loginUser, selectLoginStatus } from "../admin/userSlice";

export default function Login() {
  const dispatch = useDispatch();
  const loginStatus = useSelector(selectLoginStatus);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [loginRequestStatus, setLoginRequestStatus] = useState("idle");

  const canLogin =
    [username, password].every(Boolean) && loginRequestStatus === "idle";

  const onLogin = async () => {
    if (canLogin) {
      try {
        setLoginRequestStatus("pending");
        const resultAction = await dispatch(loginUser({ username, password }));
        unwrapResult(resultAction);
        setUsername("");
        setPassword("");
      } catch (error) {
        console.log("échec de la connexion : ", error);
      } finally {
        setLoginRequestStatus("idle");
      }
    }
  };

  const displayAlert = () => {
    switch (loginStatus) {
      case "LOGIN_SUCCESS":
        return <AlertTimeOut type="success" message="Connexion réussie !" />;

      case "LOGIN_FAILURE":
        return (
          <AlertTimeOut
            type="danger"
            message="Mauvais identifiant / mot de passe !"
          />
        );

      default:
        return null;
    }
  };

  return (
    <div>
      {loginRequestStatus === "idle" ? displayAlert() : null}
      <h2>Connexion</h2>
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Adresse Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Entrez votre adresse email"
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Mot de passe</Form.Label>
          <Form.Control
            type="password"
            placeholder="Entrez votre mot de passe"
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button variant="outline-dark" onClick={onLogin} disabled={!canLogin}>
          Connexion
        </Button>
      </Form>
    </div>
  );
}
