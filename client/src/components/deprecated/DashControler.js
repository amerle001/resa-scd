import React from "react";
import { Button } from "reactstrap";

class DeleteBib extends React.Component {
  render() {
    return (
      <div>
        <h1>Tableau de bord</h1>
        {this.props.scd.map((bib) => (
          <p className="deleteBib" key={bib._id}>
            {bib.name}
            <Button
              style={{ float: "right", marginLeft: "1em" }}
              color="info"
              size="sm"
              onClick={(e) => this.props.afficherBib(e, bib._id)}
            >
              {this.props.isDisplayed && this.props.isDisplayed === bib._id
                ? "Réduire"
                : "Afficher"}
            </Button>

            <Button
              style={{ float: "right" }}
              color="danger"
              size="sm"
              onClick={(e) => this.props.delBib(e, bib._id)}
            >
              Supprimer
            </Button>
          </p>
        ))}
      </div>
    );
  }
}

export default DeleteBib;
