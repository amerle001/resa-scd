import { configureStore } from "@reduxjs/toolkit";
import scdReducer from "./components/admin/scdSlice";
import userReducer from "./components/admin/userSlice";
import userBaseReducer from "./components/admin/userBaseSlice";
import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import thunk from "redux-thunk";
import { persistReducer } from "redux-persist";
// import hardSet from "redux-persist/lib/stateReconciler/hardSet";

const persistConfig = {
  key: "root",
  storage,
  // whitelist: ["navigation", "auth"],
  // stateReconciler: hardSet,
};

const reducers = combineReducers({
  scd: scdReducer,
  user: userReducer,
  userBase: userBaseReducer,
});

const persistedReducer = persistReducer(persistConfig, reducers);

export default configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
});

// state.scd = slice de l'état
// scdReducer = fonction pure responsable de MaJ de state.scd
