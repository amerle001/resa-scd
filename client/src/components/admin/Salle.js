import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { reservePlace, liberePlace, selectCurrentBib } from "./scdSlice";

export default function Salle(props) {
  const dispatch = useDispatch();
  const [nomReservant, setNomReservant] = useState("");
  const currentBibTemp = useSelector(selectCurrentBib);
  const currentBib = currentBibTemp[0];

  const site = currentBib.name;
  const contenance = currentBib.contenance;
  const etatSalle = currentBib.places;
  const idBib = currentBib._id;

  const [resaRequestStatus, setResaRequestStatus] = useState("idle");
  const [libereRequestStatus, setLibereRequestStatus] = useState("idle");
  const canReserve =
    nomReservant &&
    resaRequestStatus === "idle" &&
    libereRequestStatus === "idle" &&
    contenance > 0;

  const canLibere =
    resaRequestStatus === "idle" && libereRequestStatus === "idle";

  const onReserve = async () => {
    if (canReserve) {
      try {
        setResaRequestStatus("pending");
        const resultAction = await dispatch(
          reservePlace({ nomReservant, idBib, compteTemporaire: true })
        );
        unwrapResult(resultAction);
        setNomReservant("");
      } catch (err) {
        console.error("echec de la réservation : ", err);
      } finally {
        setResaRequestStatus("idle");
      }
    }
  };

  const onLibere = async (nomOccupant, idPlace, idBib, startReservation) => {
    if (canLibere && idPlace) {
      try {
        setLibereRequestStatus("pending");
        const resultAction = await dispatch(
          liberePlace({ nomOccupant, idPlace, idBib, startReservation })
        );
        unwrapResult(resultAction);
      } catch (err) {
        console.error("echec de la libération de place : ", err);
      } finally {
        setLibereRequestStatus("idle");
      }
    }
  };

  return (
    <div>
      <h2>Réservation immédiate</h2>
      <p>Bibliothèque : {site}</p>
      <p>Nombre de places disponibles : {contenance}</p>
      {contenance > 0 ? (
        <form>
          <label>
            Entrez le nom de l'usager :{"  "}
            <input
              placeholder="Txomin Etxemendi"
              value={nomReservant}
              onChange={(e) => setNomReservant(e.target.value)}
            />
            {"  "}
            <Button
              variant="outline-dark"
              onClick={onReserve}
              disabled={!canReserve}
            >
              Valider
            </Button>
          </label>
        </form>
      ) : (
        <p>Toutes les places sont prises !</p>
      )}

      <div className="wrapper">
        {etatSalle.map((place) => (
          <div key={place.numero} className={place.libre ? "libre" : "occupee"}>
            <p>{place.numero}</p>
            <p>{place.nom}</p>
            {place.libre ? (
              <p>Place libre</p>
            ) : (
              <div>
                <p>
                  Jusqu'à {new Date(place.endReservation).getHours()} h{" "}
                  {new Date(place.endReservation).getMinutes() <= 9
                    ? `0${new Date(place.endReservation).getMinutes()}`
                    : new Date(place.endReservation).getMinutes()}
                </p>
                <p>
                  <Button
                    variant="primary"
                    size="sm"
                    block
                    onClick={(e) =>
                      onLibere(
                        place.nom,
                        place._id,
                        idBib,
                        place.startReservation
                      )
                    }
                    disabled={!canLibere}
                  >
                    Libérer place
                  </Button>
                </p>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
