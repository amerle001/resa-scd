const express = require("express");
const router = express.Router();
const passport = require("passport");
require("../../config/passportConfig")(passport);
const User = require("../../models/User");
const Bibliotheque = require("../../models/Bibliotheque");

// @route   GET users/userBase
// @desc    Get tous les users inscrits. Classement montant par lastName.
// @access  PROTECTED

router.get("/", (req, res) => {
  if (req.user) {
    if (
      req.user.privilegeLevel === "admin" ||
      req.user.privilegeLevel === "superadmin"
    ) {
      console.log("req.user");

      console.log(req.user.privilegeLevel);
      User.find()
        .sort({ lastName: 1 })
        .then((users_full) => {
          // fiches users expurgées du mdp
          const users_filtered = users_full.map((user_filtered) => ({
            _id: user_filtered._id,
            privilege_level: user_filtered.privilege_level,
            is_active: user_filtered.is_active,
            firstName: user_filtered.firstName,
            lastName: user_filtered.lastName,
            username: user_filtered.username,
            register_date: user_filtered.register_date,
            reservations: user_filtered.reservations,
          }));
          res.json(users_filtered);
        })
        .catch((err) =>
          res.status(404).json({ get_users_success: false, message: err })
        );
    }
  } else {
    res.send("Accès refusé");
  }
});

// @route   DELETE users/userBase
// @desc    Supprimer un usager inscrit
// @access  PROTECTED

router.delete("/:id", (req, res) => {
  (async () => {
    console.log("DEBUG DELETE USER");
    console.log(req.user);

    const userToDelete = await User.findById(req.params.id);

    if (
      req.user &&
      (req.user.privilegeLevel === "admin" ||
        req.user.privilegeLevel === "superadmin")
    ) {
      if (req.user.id.toString() === req.params.id.toString()) {
        res.json({
          delete_success: false,
          message: "Impossible de supprimer son propre compte",
        });
        return 0;
      }

      // ******************suppression de toutes les reservations dans les bibs**************
      //****************** a refactoriser en fonction **************

      if (userToDelete) {
        for (let bibliotheque of await Bibliotheque.find()) {
          let contenance = bibliotheque.contenance;
          for (let place of bibliotheque.places) {
            if (place.startReservation && place.nom) {
              if (
                userToDelete.reservations
                  .map((resa) => resa.placeId)
                  .includes(place._id.toString()) &&
                userToDelete.reservations
                  .map((resa) => resa.start.getTime())
                  .includes(place.startReservation.getTime()) &&
                userToDelete.username.includes(place.nom)
              ) {
                console.log("test 1er niveau réussi");
                ++contenance;
                console.log("contenance :" + contenance);

                await Bibliotheque.updateOne(
                  {
                    _id: bibliotheque._id,
                    "places._id": place._id,
                  },
                  {
                    // contenance: bibliotheque.contenance + 1,
                    contenance: contenance,
                    "places.$.nom": "",
                    "places.$.startReservation": null,
                    "places.$.endReservation": null,
                    "places.$.durationInMinutes": null,
                    "places.$.libre": true,
                  }
                )
                  .catch((err) => console.log(err))
                  .then(() => {
                    console.log(
                      "ok resa 1er niveau supprimée suite à suppression de compte."
                    );
                  });
              }
            }
            for (let fResa of place.futureResa) {
              if (
                userToDelete.reservations
                  .map((resa) => resa.idNewResaFromPlace)
                  .includes(fResa._id.toString()) &&
                userToDelete.reservations
                  .map((resa) => resa.start.getTime())
                  .includes(fResa.fStartReservation.getTime()) &&
                userToDelete.username.includes(fResa.nom)
              ) {
                console.log("test 2eme niveau réussi");

                const bib = await Bibliotheque.findById(bibliotheque._id);
                const placeAvecFResaASupprimer = await bib.places.id(place._id);
                await placeAvecFResaASupprimer.futureResa.pull({
                  _id: fResa._id,
                });
                await bib
                  .save()
                  .catch((err) => console.log(err))
                  .then(() => {
                    console.log(
                      "ok fresa supprimée suite à suppression de compte."
                    );
                  });
              }
            }
          }
        }
      }
      // };

      // ******************

      console.log("req.user");
      console.log(req.user.privilegeLevel);
      console.log("req.params.id : " + req.params.id);

      User.findById(req.params.id).then((user) =>
        user
          .remove()
          .then(() => res.json({ delete_success: true, userDeleted: user }))
          .catch((err) =>
            res.status(404).json({ deleteUserSuccess: false, message: err })
          )
      );
    } else {
      res.send("Accès refusé");
    }
  })();
});

// @route   POST users/userBase
// @desc    Get tous les users inscrits. Classement montant par lastName.
// @access  PROTECTED

router.post("/", (req, res) => {
  (async () => {
    console.log("debug privilegeLevel");
    console.log(req.body);

    const {
      userId,
      userCurrentLevelOfPrivilege,
      userNewLevelOfPrivilege,
    } = req.body;
    console.log(userId);
    console.log(userCurrentLevelOfPrivilege);
    console.log(userNewLevelOfPrivilege);
    if (req.user) {
      if (
        req.user.privilegeLevel === "admin" &&
        (userCurrentLevelOfPrivilege === "user" ||
          userCurrentLevelOfPrivilege === "admin") &&
        (userNewLevelOfPrivilege === "user" ||
          userNewLevelOfPrivilege === "admin")
      ) {
        await User.findByIdAndUpdate(userId, {
          privilege_level: userNewLevelOfPrivilege,
        }).catch((err) =>
          res
            .status(500)
            .json({ user_privilege_level_updated: false, message: err })
        );
        const response = await User.findById(userId);
        res.json(response);
      }
    } else if (
      req.user.privilegeLevel === "superadmin" &&
      (userNewLevelOfPrivilege === "user" ||
        userNewLevelOfPrivilege === "admin" ||
        userNewLevelOfPrivilege === "superadmin")
    ) {
      await User.findByIdAndUpdate(userId, {
        privilege_level: userNewLevelOfPrivilege,
      }).catch((err) =>
        res
          .status(500)
          .json({ user_privilege_level_updated: false, message: err })
      );
      const response = await User.findById(userId);
      res.json(response);
    } else {
      const response = await User.findById(userId);
      res.json(response);
    }
  })();
});

module.exports = router;
