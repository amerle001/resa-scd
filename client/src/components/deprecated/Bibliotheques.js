import React, { useEffect } from "react";
import Salle from "../admin/Salle";
import Dashboard from "./Dashboard";

import { useSelector, useDispatch } from "react-redux";
import { selectScd, selectBibAffichee, fetchScd } from "../admin/scdSlice";

export default function Bibliotheques() {
  const dispatch = useDispatch();
  const scdRedux = useSelector(selectScd);
  const scdFetchStatus = useSelector((state) => state.scd.status);
  const bibAffichee = useSelector(selectBibAffichee);

  useEffect(() => {
    if (scdFetchStatus === "idle") dispatch(fetchScd());
  }, [scdFetchStatus, dispatch]);

  return (
    <div>
      <Dashboard scd={scdRedux} />
      {bibAffichee === null ? (
        <p>Veuillez sélectionner une bibliothèque à afficher.</p>
      ) : (
        <Salle />
      )}
    </div>
  );
}
