import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { fetchUserBase, selectUserBase, deleteUser } from "./userBaseSlice";
import { selectUser } from "./userSlice";
import Button from "react-bootstrap/Button";
import { formatDateForHumans } from "./UserBase";

export default function UserSearch(props) {
  const dispatch = useDispatch();
  const userData = useSelector(selectUser);
  const userBase = useSelector(selectUserBase);
  const [nomRecherche, setNomrecherche] = useState(null);

  useEffect(() => {
    const resultAction = dispatch(fetchUserBase());
    unwrapResult(resultAction);
    console.log(userBase);
  }, []);

  return (
    <div>
      {userData ? (
        <div>
          <h3>Sélectionner un utilisateur</h3>
          <form>
            <label>
              Rechercher par nom de famille :{"  "}
              <input
                placeholder="Dupont"
                value={nomRecherche}
                onChange={(e) => setNomrecherche(e.target.value)}
              />
              {"  "}
            </label>
          </form>
        </div>
      ) : null}

      <div className="wrapper">
        {userData && userBase && userBase !== "Accès refusé"
          ? userBase
              .filter((user) =>
                nomRecherche
                  ? user.lastName
                      .toLowerCase()
                      .indexOf(nomRecherche.toLowerCase()) >= 0
                    ? user
                    : null
                  : user.lastName
              )
              .map((user) => (
                <div className="libre" key={user._id}>
                  <p>
                    <strong>Nom d'utilisateur :</strong> {user.username}
                  </p>
                  <p>
                    <strong>Prénom :</strong> {user.firstName}
                  </p>
                  <p>
                    <strong>Nom :</strong> {user.lastName}
                  </p>
                  <p>
                    <strong>id :</strong> {user._id}
                  </p>
                  <p>
                    <strong>privilege :</strong> {user.privilege_level}
                  </p>
                  <p>
                    <strong>Date d'inscription :</strong>{" "}
                    {formatDateForHumans(user.register_date)}
                  </p>
                  <Button
                    type="button"
                    variant="outline-dark"
                    size="sm"
                    onClick={(e) => {
                      e.preventDefault();
                      props.onSelect(user);
                    }}
                  >
                    Sélectionner
                  </Button>
                </div>
              ))
          : null}
        <br />
      </div>
    </div>
  );
}
