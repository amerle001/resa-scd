import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import AlertTimeOut from "../public/AlertTimeOut";
import { useSelector, useDispatch } from "react-redux";
import {
  selectScd,
  programResa,
  resetProgramResaStatus,
} from "../admin/scdSlice";
import { selectUser, getUser } from "../admin/userSlice";
import { unwrapResult } from "@reduxjs/toolkit";

const boolFormResaEstPerimee = (timeStamp) => {
  if (timeStamp < Date.now()) {
    return true;
  }
  return false;
};

export default function ProgramResa() {
  const dispatch = useDispatch();
  const scd = useSelector(selectScd);
  const userData = useSelector(selectUser);
  const programResaStatus = useSelector((state) => state.scd.programResaStatus);
  const programResaMessage = useSelector(
    (state) => state.scd.programResaMessage
  );

  const [startResa, setStartResa] = useState(null);
  const [dureeResaHeures, setdureeResaHeures] = useState(null);
  const [lieu, setLieu] = useState(null);

  const [resaRequestStatus, setResaRequestStatus] = useState("idle");
  const [resaSechevauchent, setResaSeChevauchent] = useState(false);
  const [resaPerimee, setResaPerimee] = useState(false);

  useEffect(() => {
    dispatch(resetProgramResaStatus());
  }, []);

  const canSubmit =
    startResa &&
    // on vérifie que le nombre d'heure est un chiffre > 0
    !isNaN(dureeResaHeures) &&
    dureeResaHeures > 0 &&
    // on vérifie que le formulaire n'est pas resté sur "Bibibliothèque"
    (() => {
      const tempScd = scd.map((bib) => bib._id);
      return tempScd.includes(lieu);
    })() &&
    resaRequestStatus === "idle";

  const onSubmit = async (e) => {
    e.preventDefault();
    if (canSubmit) {
      const dureeResaMinutes = dureeResaHeures * 60;
      const formStartResa = startResa.getTime();
      const formEndResa = startResa.getTime() + dureeResaMinutes * 60 * 1000;

      // avant d'envoyer la requête au serveur on vérifie :
      // si la resa chevauche une autre que l'usager aurait déja faite
      // si la resa n'est pas déjà périmée (concerne une plage horaire déjà révolue)

      if (
        boolResaSeChevauchent(formStartResa, formEndResa) ||
        boolFormResaEstPerimee(formStartResa)
      ) {
        if (boolResaSeChevauchent(formStartResa, formEndResa)) {
          setResaSeChevauchent(true);
          setTimeout(() => {
            setResaSeChevauchent(false);
          }, 2000);
        } else {
          setResaPerimee(true);
          setTimeout(() => {
            setResaPerimee(false);
          }, 2000);
        }
      } else {
        try {
          setResaRequestStatus("pending");
          const resultAction = await dispatch(
            programResa({
              idBib: lieu,
              nomReservant: userData.username,
              startResa: formStartResa,
              endResa: formEndResa,
              dureeResaMinutes,
            })
          );
          unwrapResult(resultAction);
          console.log(resultAction);
          displayAlert();
          setTimeout(() => {
            dispatch(resetProgramResaStatus());
          }, 2000);
        } catch (error) {
          console.log(error);
        } finally {
          setResaRequestStatus("idle");
          setStartResa(null);
          setdureeResaHeures(null);
          setLieu(null);
        }
      }
    }
  };

  const displayAlert = () => {
    return <AlertTimeOut type="light" message={programResaMessage} />;
  };

  const displayProblemeResa = (message) => {
    return <AlertTimeOut type="light" message={message} />;
  };

  const boolResaSeChevauchent = (formStartResa, formEndResa) => {
    for (let userFResa of userData.reservations) {
      console.log(userFResa);
      console.log(`${formStartResa} >= ${new Date(userFResa.start).getTime()}`);
      console.log(`$--`);
      console.log(
        `${formEndResa} <= ${
          new Date(userFResa.start).getTime() +
          userFResa.durationInMinutes * 60 * 1000
        }`
      );

      if (
        (formStartResa >= new Date(userFResa.start).getTime() &&
          formEndResa <=
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000) ||
        (formStartResa <=
          new Date(userFResa.start).getTime() +
            userFResa.durationInMinutes * 60 * 1000 &&
          formEndResa >=
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000) ||
        (formStartResa <= new Date(userFResa.start).getTime() &&
          formEndResa >= new Date(userFResa.start).getTime()) ||
        (formStartResa == new Date(userFResa.start).getTime() &&
          formEndResa ==
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000)
      ) {
        console.log("a déjà une autre resa a cet horaire");

        return true;
      }
      return false;
    }
  };

  return (
    <div>
      <h3>Programmer une réservation</h3>
      <Form>
        <Form.Group>
          <Form.Label>Choisissez votre bibliothèque</Form.Label>
          <Form.Control
            as="select"
            className="mr-sm-2"
            id="inlineFormCustomSelect"
            custom
            onChange={(e) => setLieu(e.target.value)}
          >
            <option value="0">Bibliothèque</option>
            {scd.map((bib) => (
              <option key={bib._id} value={bib._id}>
                {bib.name}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group>
          <Form.Label>Choisissez le jour et l'heure</Form.Label>
          <Form.Control
            type="datetime-local"
            onChange={(e) => setStartResa(new Date(e.target.value))}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Combien de temps ?</Form.Label>
          <Form.Control
            as="select"
            className="my-1 mr-sm-2"
            id="inlineFormCustomSelectPref"
            custom
            onChange={(e) => setdureeResaHeures(e.target.value)}
          >
            <option value="0">Choisissez...</option>
            <option value="1">1h</option>
            <option value="2">2h</option>
            <option value="3">3h</option>
            <option value="4">4h</option>
          </Form.Control>
        </Form.Group>
        <Button
          variant="outline-dark"
          type="submit"
          disabled={!canSubmit}
          onClick={onSubmit}
        >
          Soumettre
        </Button>
      </Form>
      {resaRequestStatus === "idle" && programResaMessage
        ? displayAlert()
        : null}
      {resaPerimee
        ? displayProblemeResa("Impossible de réserver une place dans le passé.")
        : null}
    </div>
  );
}
