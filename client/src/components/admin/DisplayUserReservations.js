import React, { useState, useEffect } from "react";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import { formatDateForHumans } from "./UserBase";
import Button from "react-bootstrap/Button";
import { unwrapResult } from "@reduxjs/toolkit";
import { useSelector, useDispatch } from "react-redux";
import { deleteResa } from "../admin/scdSlice";
import { fetchUserBase } from "./userBaseSlice";

export default function DisplayUserReservations(props) {
  const dispatch = useDispatch();

  const [deleteRequestStatus, setDeleteRequestStatus] = useState("idle");
  const canDelete = deleteRequestStatus === "idle";

  const onDelete = async (infoDeleteFResa) => {
    if (canDelete) {
      try {
        setDeleteRequestStatus("pending");
        const resultAction = await dispatch(deleteResa(infoDeleteFResa));
        unwrapResult(resultAction);
        console.log(resultAction);
        // const i = props.suppressionResaCounter + 1;
        // props.setSuppressionResaCounter(i);
        // await dispatch(getUser());
        const fetchUserBaseAction = await dispatch(fetchUserBase());
        unwrapResult(fetchUserBaseAction);
      } catch (error) {
        console.log(error);
      } finally {
        setDeleteRequestStatus("idle");
      }
    }
  };

  return (
    <div>
      <Accordion className="accordeon">
        <Card>
          <Accordion.Toggle as={Card.Header} variant="link" eventKey="0">
            {props.arrayReservations.length === 0
              ? "Aucune réservation"
              : "Réservations"}
          </Accordion.Toggle>

          <Accordion.Collapse eventKey="0">
            <Card.Body>
              {props.arrayReservations.length === 0
                ? null
                : props.arrayReservations.map((reservation) => (
                    <div className="fResa" key={reservation._id}>
                      <div className="accordeon">
                        <Accordion>
                          <Card>
                            <Accordion.Toggle
                              as={Card.Header}
                              variant="link"
                              eventKey="0"
                            >
                              {reservation.locationName}
                              {"  "}
                              {formatDateForHumans(reservation.start)}
                            </Accordion.Toggle>

                            <Accordion.Collapse eventKey="0">
                              <Card.Body>
                                <p>
                                  <strong>Lieu : </strong>
                                  {reservation.locationName}
                                </p>

                                <p>
                                  <strong>Date : </strong>
                                  {formatDateForHumans(reservation.start)}
                                </p>
                                <p>
                                  <strong>Heure : </strong>{" "}
                                  {new Date(reservation.start).getHours()}:
                                  {new Date(reservation.start).getMinutes()}
                                </p>

                                <p>
                                  <strong>Durée (minutes) : </strong>
                                  {reservation.durationInMinutes}
                                </p>
                                <Button
                                  variant="outline-dark"
                                  size="sm"
                                  block
                                  onClick={(e) =>
                                    onDelete({
                                      nomReservant: props.username,
                                      idBib: reservation.locationId,
                                      idPlace: reservation.placeId,
                                      fResaId: reservation.idNewResaFromPlace,
                                      userResaId: reservation._id,
                                    })
                                  }
                                  disabled={!canDelete}
                                >
                                  Supprimer
                                </Button>
                              </Card.Body>
                            </Accordion.Collapse>
                          </Card>
                        </Accordion>
                      </div>
                    </div>
                  ))}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>
  );
}
