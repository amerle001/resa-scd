import React, { Component } from "react";
import { Button } from "reactstrap";

export class Salle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.reservePlace = this.reservePlace.bind(this);
  }

  handleChange(e) {
    this.setState({ nom: e.target.value });
  }

  reservePlace(e) {
    const infosResa = {
      nomReservant: this.state.nom,
      bibId: this.props.idBib,
    };
    this.props.reservePlace(e, infosResa);
    this.setState({ nom: "" });
  }

  render() {
    return (
      <div>
        <p>Bibliothèque de : {this.props.site}</p>
        <p>Nombre de places disponibles : {this.props.contenance}</p>
        <form onSubmit={this.reservePlace}>
          <label>
            Entrez votre nom :
            <input
              placeholder="Txomin Etxemendi"
              value={this.state.nom}
              onChange={(e) => this.handleChange(e)}
            />
            <input type="submit" value="Réserver" />
          </label>
        </form>
        <div className="wrapper">
          {this.props.etatSalle.map((place) => (
            <div
              key={place.numero}
              className={place.libre ? "libre" : "occupee"}
            >
              <p>{place.numero}</p>
              <p>{place.nom}</p>
              {place.libre ? (
                <p>Place libre</p>
              ) : (
                <p>
                  <Button
                    color="primary"
                    size="sm"
                    block
                    onClick={(e) =>
                      this.props.liberePlace(e, place._id, this.props.idBib)
                    }
                  >
                    Libérer place
                  </Button>
                </p>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Salle;
