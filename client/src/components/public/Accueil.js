import React from "react";
import Button from "react-bootstrap/Button";
import { selectUser } from "../admin/userSlice";
import { useSelector } from "react-redux";

export default function Accueil({ history }) {
  const userData = useSelector(selectUser);
  const greetings = () => {
    return userData ? (
      <div>
        <p>Bienvenue, {`${userData.firstName} ${userData.lastName}`}.</p>
        {bonneVue()}
      </div>
    ) : (
      <div>
        <Button
          variant="outline-dark"
          onClick={() => {
            history.push("/login");
          }}
        >
          Se connecter
        </Button>
        {"  "}
        <Button
          variant="outline-dark"
          onClick={() => history.push("/register")}
        >
          S'inscrire
        </Button>
      </div>
    );
  };

  const boutonCommencer = (route) => {
    return (
      <Button
        variant="outline-dark"
        onClick={() => {
          history.push(route);
        }}
      >
        Commencer
      </Button>
    );
  };

  const bonneVue = () => {
    if (
      userData.privilegeLevel === "admin" ||
      userData.privilegeLevel === "superadmin"
    ) {
      return boutonCommencer("/admin");
    } else if (userData.privilegeLevel === "user") {
      return boutonCommencer("/userView");
    }
  };

  return (
    <div>
      <div className="accueil-container">
        <div className="left">
          {" "}
          <h1>Je réserve ma place à la bibliothèque.</h1>
          {greetings()}
        </div>
        <div className="right">
          <img className="logo-accueil" src="./logo4.png"></img>
        </div>
      </div>
    </div>
  );
}
