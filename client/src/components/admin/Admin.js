import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

import { selectBibAffichee, fetchScd } from "./scdSlice";

import { selectUser } from "./userSlice";
import DashControl from "./DashControl";
import Salle from "./Salle";
import DashCreateBib from "./DashCreateBib";

export default function Admin() {
  const userData = useSelector(selectUser);
  const history = useHistory();
  const dispatch = useDispatch();
  const scdFetchStatus = useSelector((state) => state.scd.status);
  const bibAffichee = useSelector(selectBibAffichee);

  useEffect(() => {
    dispatch(fetchScd());
  }, [dispatch, bibAffichee]);

  // toutes les minutes, rafraichit la salle avec un fetchScd
  // note : ancienne version n'a pas de setInterval
  useEffect(() => {
    setInterval(() => {
      if (scdFetchStatus === "idle") dispatch(fetchScd());
    }, 60000);
  }, [scdFetchStatus, dispatch]);

  useEffect(() => {
    if (!userData) {
      setTimeout(() => {
        history.push("/");
      }, 2000);
    }
  }, []);

  return (
    <div>
      {userData ? (
        <div>
          <div className="admin-container">
            <div className="admin-sidebar">
              <h2>Tableau de bord</h2>
              <p>Etat des salles, réservation immédiate</p>
              <DashControl />
              <DashCreateBib />
              <h3>
                <Link className="Link" to="/adminProgramView">
                  Programmer une réservation
                </Link>
              </h3>
              <br />
              <h3>
                <Link className="Link" to="/userBase">
                  Rechercher un utilisateur
                </Link>
              </h3>
              <br />
              <h3>
                <Link className="Link" to="/configuration">
                  Configurer l'application
                </Link>
              </h3>
            </div>
            <div className="admin-content">
              {bibAffichee === null ? (
                <h2>Veuillez sélectionner une bibliothèque à afficher.</h2>
              ) : (
                <Salle />
              )}
            </div>
          </div>
        </div>
      ) : (
        <h1>Connectez-vous pour accéder à cette page.</h1>
      )}
    </div>
  );
}
