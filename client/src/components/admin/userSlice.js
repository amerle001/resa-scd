// @desc    Rassemble la logique liée à l'utilisateur connecté

import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  user: null,
  registerStatus: null,
  loginStatus: null,
  logoutStatus: null,
  message: null,
  status: "idle",
  error: null,
};

export const getUser = createAsyncThunk("users/getUser", async () => {
  const response = await axios.get("/users/getUser", { withCredentials: true });
  return response.data;
});

export const loginUser = createAsyncThunk(
  "users/login",
  async (credentials) => {
    const response = await axios.post("/users/login", credentials, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const logoutUser = createAsyncThunk("users/logout", async () => {
  const response = await axios.get("/users/logout", { withCredentials: true });
  return response.data;
});

export const registerUser = createAsyncThunk(
  "users/register",
  async (userInfo) => {
    const response = await axios.post("/users/register", userInfo, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    [loginUser.fulfilled]: (state, action) => {
      if (action.payload === "LOGIN_FAILURE") {
        state.loginStatus = "LOGIN_FAILURE";
      } else {
        state.user = action.payload;
        state.loginStatus = "LOGIN_SUCCESS";
        state.logoutStatus = null;
      }
    },
    [logoutUser.fulfilled]: (state, action) => {
      state.logoutStatus = "LOGOUT_SUCCESS";
      // state.loginStatus = state.user = state.registerStatus = state.loginStatus = state.logoutStatus = state.message = state.status = state.error = null;
      state.loginStatus = state.user = state.registerStatus = state.error = state.status = null;
      state.message = action.payload;
    },
    [getUser.fulfilled]: (state, action) => {
      state.user = action.payload;
    },
    [registerUser.fulfilled]: (state, action) => {
      if (action.payload.status === "REGISTER_FAILURE") {
        state.registerStatus = "REGISTER_FAILURE";
        state.message = action.payload.message;
      } else {
        state.registerStatus = "REGISTER_SUCCESS";
      }
    },
  },
});

export default userSlice.reducer;

// SELECTORS
export const selectUser = (state) => state.user.user;
export const selectLoginStatus = (state) => state.user.loginStatus;
export const selectLogoutStatus = (state) => state.user.logoutStatus;
export const selectRegisterStatus = (state) => state.user.registerStatus;
export const selectStatusMessage = (state) => state.user.message;
