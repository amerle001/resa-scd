import React from "react";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import Login from "../public/Login";

// s'affiche quand on n'est pas connecté. Page de base.

export default function About({ history }) {
  return (
    <div className="About">
      <div className="left">
        {" "}
        <h1>Je réserve ma place à la bibliothèque.</h1>
        <Button
          variant="outline-dark"
          onClick={() => {
            history.push("/login");
          }}
        >
          {/* <Link to="/login">Se connecter</Link> */}
          Se connecter
        </Button>
        {"  "}
        <Button variant="outline-dark">
          <Link to="/register">S'inscrire</Link>
        </Button>
      </div>
      <div className="right">
        <img src="./logo4.png"></img>
      </div>
    </div>
  );
}
