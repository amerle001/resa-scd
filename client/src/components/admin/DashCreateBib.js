import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";

import { postNewBibliotheque } from "./scdSlice";

export default function DashCreateBib() {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [contenance, setContenance] = useState(0);
  const [postRequestStatus, setPostRequestStatus] = useState("idle");

  const canPost =
    [name, contenance].every(Boolean) && postRequestStatus === "idle";

  const handleNewBibSubmit = async () => {
    console.log("can post : " + canPost);

    if (canPost) {
      try {
        setPostRequestStatus("pending");
        const resultAction = await dispatch(
          postNewBibliotheque({ name, contenance })
        );
        unwrapResult(resultAction);
        setName("");
        setContenance(0);
      } catch (err) {
        console.error("echec du post de la nouvelle bibliotheque: ", err);
      } finally {
        setPostRequestStatus("idle");
      }
    }
  };

  return (
    <div>
      <h3>Créer une nouvelle bibliothèque</h3>
      <form>
        <label>
          Entrez le nom de la bibliothèque à créer :
          <input
            placeholder="nom BU"
            type="String"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </label>
        <label>
          Entrez son nombre de places assises :
          <input
            type="Number"
            value={contenance}
            onChange={(e) => setContenance(e.target.value)}
          />
        </label>
        <button type="button" onClick={handleNewBibSubmit} disabled={!canPost}>
          Valider
        </button>
      </form>
    </div>
  );
}
