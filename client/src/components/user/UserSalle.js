import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import AlertTimeOut from "../public/AlertTimeOut";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  reservePlace,
  liberePlaceUser,
  selectCurrentBib,
} from "../admin/scdSlice";
import { selectUser, getUser } from "../admin/userSlice";

const isUserInSalle = (username, salle) => {
  return salle
    .map((places) => places.nom)
    .filter((nom) => nom === username)
    .includes(username);
};

export default function UserSalle(props) {
  const dispatch = useDispatch();
  const currentBibTemp = useSelector(selectCurrentBib);
  const currentBib = currentBibTemp[0];
  const userData = useSelector(selectUser);

  const site = currentBib.name;
  const contenance = currentBib.contenance;
  const etatSalle = currentBib.places;
  const idBib = currentBib._id;

  const [resaRequestStatus, setResaRequestStatus] = useState("idle");
  const [libereRequestStatus, setLibereRequestStatus] = useState("idle");
  const [resaSechevauchent, setResaSeChevauchent] = useState(false);

  const canReserve =
    userData.has_already_a_seat === false &&
    resaRequestStatus === "idle" &&
    libereRequestStatus === "idle" &&
    contenance > 0;

  const canLibere =
    resaRequestStatus === "idle" &&
    libereRequestStatus === "idle" &&
    userData.has_already_a_seat === true &&
    isUserInSalle(userData.username, etatSalle);

  const onReserve = async (e) => {
    e.preventDefault();
    if (canReserve) {
      // avant d'envoyer la requête au serveur on vérifie :
      // si la resa chevauche une autre que l'usager aurait déja faite

      const startResa = Date.now();
      const endResa = startResa + 3600000; // plus 1 heure

      if (boolResaSeChevauchent(startResa, endResa)) {
        setResaSeChevauchent(true);
        setTimeout(() => {
          setResaSeChevauchent(false);
        }, 2000);
      } else {
        try {
          setResaRequestStatus("pending");
          const resultAction = await dispatch(
            reservePlace({ nomReservant: userData.username, idBib })
          );
          unwrapResult(resultAction);
          const response = await dispatch(getUser());
          console.log(response);
        } catch (err) {
          console.error("echec de la réservation : ", err);
        } finally {
          setResaRequestStatus("idle");
        }
      }
    }
  };

  const onLibere = async (idBib) => {
    if (canLibere) {
      try {
        setLibereRequestStatus("pending");
        const resultAction = await dispatch(
          liberePlaceUser({
            nomLiberant: userData.username,
            idBib: currentBib._id,
          })
          // liberePlaceUser({ nomLiberant: "tartanpion" })
        );
        unwrapResult(resultAction);
        const response = await dispatch(getUser());
        console.log(response);
      } catch (err) {
        console.error("echec de la libération de place : ", err);
      } finally {
        setLibereRequestStatus("idle");
      }
    }
  };

  const boolResaSeChevauchent = (formStartResa, formEndResa) => {
    for (let userFResa of userData.reservations) {
      console.log(userFResa);
      console.log(`${formStartResa} >= ${new Date(userFResa.start).getTime()}`);
      console.log(`$--`);
      console.log(
        `${formEndResa} <= ${
          new Date(userFResa.start).getTime() +
          userFResa.durationInMinutes * 60 * 1000
        }`
      );

      if (
        (formStartResa >= new Date(userFResa.start).getTime() &&
          formEndResa <=
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000) ||
        (formStartResa <=
          new Date(userFResa.start).getTime() +
            userFResa.durationInMinutes * 60 * 1000 &&
          formEndResa >=
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000) ||
        (formStartResa <= new Date(userFResa.start).getTime() &&
          formEndResa >= new Date(userFResa.start).getTime()) ||
        (formStartResa == new Date(userFResa.start).getTime() &&
          formEndResa ==
            new Date(userFResa.start).getTime() +
              userFResa.durationInMinutes * 60 * 1000)
      ) {
        console.log("a déjà une autre resa a cet horaire");

        return true;
      }
      return false;
    }
  };

  const displayProblemeResa = (message) => {
    return <AlertTimeOut type="danger" message={message} />;
  };

  useEffect(() => {
    dispatch(getUser());
  }, []);

  return (
    <div>
      <h2>Réservation immédiate</h2>
      <p>Bibliothèque : {site}</p>
      <p>Nombre de places disponibles : {contenance}</p>{" "}
      {contenance > 0 ? (
        userData.has_already_a_seat ? (
          <Button
            variant="outline-dark"
            onClick={onLibere}
            disabled={!canLibere}
          >
            Libérer
          </Button>
        ) : (
          <Button
            variant="outline-dark"
            onClick={onReserve}
            disabled={!canReserve}
          >
            Réserver
          </Button>
        )
      ) : (
        <p>Toutes les places sont prises !</p>
      )}
      {resaSechevauchent
        ? displayProblemeResa(
            "Impossible, vous avez déjà une réservation sur créneau."
          )
        : null}
      <div className="wrapper">
        {etatSalle.map((place) => (
          <div key={place.numero} className={place.libre ? "libre" : "occupee"}>
            <p>{place.numero}</p>

            {place.libre ? (
              <p>Place libre</p>
            ) : (
              <div>
                <p>{place.nom === userData.username ? place.nom : ""}</p>
                <p>
                  Jusqu'à {new Date(place.endReservation).getHours()} h{" "}
                  {new Date(place.endReservation).getMinutes() <= 9
                    ? `0${new Date(place.endReservation).getMinutes()}`
                    : new Date(place.endReservation).getMinutes()}
                </p>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
