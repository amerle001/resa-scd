import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchUserBase,
  selectUserBase,
  deleteUser,
  modifyPrivilegeLevel,
} from "./userBaseSlice";
import { selectUser } from "./userSlice";
import { unwrapResult } from "@reduxjs/toolkit";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import DisplayModal from "../lib/DisplayModal";
import DisplayUserReservations from "./DisplayUserReservations";

// récupérer un array sorted d'une route serveur

export const formatDateForHumans = (date) => {
  return `${new Date(date).getDate()}/${
    new Date(date).getMonth() + 1 < 10
      ? "0" + (new Date(date).getMonth() + 1)
      : new Date(date).getMonth()
  }/${new Date(date).getFullYear()}`;
};

export default function UserBase() {
  const dispatch = useDispatch();
  const userData = useSelector(selectUser);
  const userBase = useSelector(selectUserBase);
  const [nomRecherche, setNomrecherche] = useState("");
  const [newPrivilegeLevel, setNewPrivilegeLevel] = useState("");
  const userBaseFetchStatus = useSelector(
    (state) => state.userBase.fetchStatus
  );
  const deleteUserStatus = useSelector(
    (state) => state.userBase.deleteUserStatus
  );
  const canDelete =
    deleteUserStatus === "idle" ||
    deleteUserStatus === "fulfilled" ||
    deleteUserStatus === "failed";

  const canFetchUserBase =
    userBaseFetchStatus === "idle" || userBaseFetchStatus === "succeeded";

  const canModifyPrivilegeLevel =
    userBaseFetchStatus === "idle" || userBaseFetchStatus === "succeeded";

  const onModifyPrivilegeLevel = async (
    e,
    userId,
    userCurrentLevelOfPrivilege,
    userNewLevelOfPrivilege
  ) => {
    e.preventDefault();
    const modifyPrivilegeData = {
      userId,
      userCurrentLevelOfPrivilege,
      userNewLevelOfPrivilege,
    };
    // console.log(userId);
    // console.log(userCurrentLevelOfPrivilege);
    // console.log(userNewLevelOfPrivilege);
    if (canModifyPrivilegeLevel) {
      try {
        await dispatch(modifyPrivilegeLevel(modifyPrivilegeData));

        // unwrapResult(resultAction);
      } catch (error) {
        console.error("echec update privilegeLevel: ", error);
      }
    }
  };

  const onDeleteUser = async (userId) => {
    if (canDelete) {
      try {
        dispatch(deleteUser(userId));
      } catch (error) {
        console.error("echec du delete de l'usager: ", error);
      }
    }
  };

  // Note : est-ce nécessaire maintenant que le chargement de la base se fait avec la page ?
  const onRefreshUI = async () => {
    if (canFetchUserBase) {
      try {
        console.log("refreshing...");
        const resultAction = dispatch(fetchUserBase());
        unwrapResult(resultAction);
      } catch (error) {
        console.log("echec fetchUserBase : " + error);
      }
    }
  };

  useEffect(() => {
    // if (userBaseFetchStatus === "idle" || userBaseFetchStatus === "succeeded") {
    const resultAction = dispatch(fetchUserBase());
    unwrapResult(resultAction);
    // }
    console.log(userBase);
  }, []);

  return (
    <div className="userbase-container">
      {userData && userBase && userBase !== "Accès refusé" ? (
        <div>
          <form>
            <label>
              Rechercher nom :{"  "}
              <input
                placeholder="Albertine"
                value={nomRecherche}
                onChange={(e) => setNomrecherche(e.target.value)}
              />
              {"  "}
            </label>
          </form>
          <Button
            type="button"
            variant="outline-dark"
            size="sm"
            onClick={(e) => onRefreshUI()}
            disabled={!canFetchUserBase}
          >
            Rafraîchir
          </Button>

          <div className="wrapper">
            {(userData.privilege_level === "superadmin"
              ? userBase
              : userBase.filter(
                  (user) =>
                    user.privilege_level === "user" ||
                    user.privilege_level === "admin"
                )
            )
              .filter((user) =>
                nomRecherche
                  ? user.lastName
                      .toLowerCase()
                      .indexOf(nomRecherche.toLowerCase()) >= 0
                    ? user
                    : null
                  : user.lastName
              )
              .map((user) => (
                <div className="libre" key={user._id}>
                  <p>
                    <strong>Username :</strong> {user.username}
                  </p>
                  <p>
                    <strong>Prénom :</strong> {user.firstName}
                  </p>
                  <p>
                    <strong>Nom :</strong> {user.lastName}
                  </p>
                  <p>
                    <strong>Id :</strong> {user._id}
                  </p>
                  <p>
                    <strong>Privilege :</strong>

                    <Form>
                      <Form.Row className="align-items-center">
                        <Col xs="auto" className="my-1">
                          <Form.Control
                            as="select"
                            className="mr-sm-2"
                            id="inlineFormCustomSelect"
                            custom
                            onChange={(e) =>
                              setNewPrivilegeLevel(e.target.value)
                            }
                          >
                            <option value={user.privilege_level}>
                              {user.privilege_level}
                            </option>
                            <option value="user">user</option>

                            <option value="admin">admin</option>

                            {userData.privilege_level === "superadmin" ? (
                              <option value="superadmin">superadmin</option>
                            ) : null}
                          </Form.Control>
                        </Col>
                        <Col xs="auto" className="my-1">
                          <Button
                            size="sm"
                            variant="outline-dark"
                            disabled={!canModifyPrivilegeLevel}
                            onClick={(e) => {
                              onModifyPrivilegeLevel(
                                e,
                                user._id,
                                user.privilege_level,
                                newPrivilegeLevel
                              );
                              // console.log(user._id);
                              // console.log(user.privilege_level);
                              // console.log(newPrivilegeLevel);
                            }}
                          >
                            Valider
                          </Button>
                        </Col>
                      </Form.Row>
                    </Form>
                  </p>
                  <p>
                    <strong>Compte actif :</strong>{" "}
                    {user.is_active ? "oui" : "non"}
                  </p>
                  <p>
                    <strong>Date d'inscription :</strong>{" "}
                    {formatDateForHumans(user.register_date)}
                  </p>

                  <DisplayUserReservations
                    arrayReservations={user.reservations}
                    username={user.username}
                  />

                  <DisplayModal
                    buttonStyle="outline-danger"
                    buttonContent="Supprimer"
                    modalHeadingContent="Supprimer le compte utilisateur"
                    modalBodyContent="Attention, toutes les informations rattachées au compte (places, réservations) seront définitivement perdues si vous le supprimez."
                    action={onDeleteUser}
                    actionParameter={user._id}
                    canTrigger={!canDelete}
                  />
                  {"  "}
                </div>
              ))}
            <br />
          </div>
        </div>
      ) : (
        <h1>Connectez-vous pour accéder à cette page.</h1>
      )}
    </div>
  );
}
