import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  selectScd,
  selectBibAffichee,
  afficherBibliotheque,
  deleteBibliotheque,
} from "./scdSlice";
import Button from "react-bootstrap/Button";
import DisplayModal from "../lib/DisplayModal";

export default function DashControl() {
  const dispatch = useDispatch();
  const scdRedux = useSelector(selectScd);
  const bibAffichee = useSelector(selectBibAffichee);
  const [deleteRequestStatus, setDeleteRequestStatus] = useState("idle");
  const canDelete = deleteRequestStatus === "idle";

  const onDeleteBib = async (bibId) => {
    if (canDelete) {
      try {
        setDeleteRequestStatus("pending");
        const resultAction = await dispatch(deleteBibliotheque(bibId));
        unwrapResult(resultAction);
      } catch (error) {
        console.error("echec du delete de la bibliotheque: ", error);
      } finally {
        setDeleteRequestStatus("idle");
      }
    }
  };

  return (
    <div>
      {scdRedux.map((bib) => (
        <div className="dashboard-row" key={bib._id}>
          <div className="dash-bib-name">{bib.name}</div>
          <div className="dashControl-buttons">
            <Button
              className="dashControl-firstButton"
              variant="outline-dark"
              size="sm"
              onClick={(e) => dispatch(afficherBibliotheque(bib._id))}
            >
              {bibAffichee
                ? bibAffichee._id === bib._id
                  ? "Réduire"
                  : "Afficher"
                : "Afficher"}
            </Button>
            <p>{"   "}</p>
            {/* <Button
              type="button"
              variant="outline-danger"
              size="sm"
              onClick={(e) => onDeleteBib(bib._id)}
              disabled={!canDelete}
            >
              Supprimer
            </Button> */}
            <DisplayModal
              buttonStyle="outline-danger"
              buttonContent="Supprimer"
              modalHeadingContent="Supprimer une bibliothèque"
              modalBodyContent="Attention, toutes les informations rattachées à la bibliothèque (places, réservations) seront définitivement perdues si vous la supprimez."
              action={onDeleteBib}
              actionParameter={bib._id}
              canTrigger={!canDelete}
            />
          </div>
        </div>
      ))}
    </div>
  );
}
