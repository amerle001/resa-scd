import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { selectBibAffichee, fetchScd } from "../admin/scdSlice";
import { Link } from "react-router-dom";

import { selectUser } from "../admin/userSlice";
import UserDashControl from "./UserDashControl";
import UserSalle from "./UserSalle";

export default function UserView() {
  const userData = useSelector(selectUser);
  const history = useHistory();
  const dispatch = useDispatch();
  const scdFetchStatus = useSelector((state) => state.scd.status);
  const bibAffichee = useSelector(selectBibAffichee);

  // afficher /réduire permet de rafraichir l'état de la salle
  useEffect(() => {
    dispatch(fetchScd());
  }, [dispatch, bibAffichee]);

  // useEffect(() => {
  //   if (scdFetchStatus === "idle") dispatch(fetchScd());
  // }, [scdFetchStatus, dispatch]);

  // toutes les minutes, rafraichit la salle avec un fetchScd
  // note : ancienne version n'a pas de setInterval
  useEffect(() => {
    setInterval(() => {
      if (scdFetchStatus === "idle") dispatch(fetchScd());
    }, 60000);
  }, [scdFetchStatus, dispatch]);

  useEffect(() => {
    if (!userData) {
      setTimeout(() => {
        history.push("/");
      }, 2000);
    }
  }, []);

  return (
    <div>
      {userData ? (
        <div>
          <div className="admin-container">
            <div className="admin-sidebar">
              <h2>J'ai besoin d'une place</h2>
              <p>Je suis déjà à la bibliothèque</p>
              <UserDashControl />
              <h3>
                <Link className="Link" to="/userProgramView">
                  Réserver pour plus tard
                </Link>
              </h3>
              <p>
                Je planifie une réservation, consulte celles que j'ai déjà
                faites
              </p>
            </div>
            <div className="admin-content">
              {bibAffichee === null ? (
                <h2>Veuillez sélectionner une bibliothèque à afficher.</h2>
              ) : (
                <UserSalle />
              )}
            </div>
          </div>
        </div>
      ) : (
        <h1>Connectez-vous pour accéder à cette page.</h1>
      )}
    </div>
  );
}
