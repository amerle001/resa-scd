const express = require("express");
const router = express.Router();

// Bibliotheques Model
const Bibliotheque = require("../../models/Bibliotheque");

// @route   POST api/configure
// @desc    Configure bibliothèques
// @access  PROTECTED

router.post("/", (req, res) => {
  (async () => {
    console.log("debug enter POST api/configure");

    console.log(req.body);
    const {
      idBib,
      defaultResaDurationHours,
      openingHourMonday,
      openingHourTuesday,
      openingHourWednesday,
      openingHourThursday,
      openingHourFriday,
      openingHourSaturday,
      openingHourSunday,
      closingHourMonday,
      closingHourTuesday,
      closingHourWednesday,
      closingHourThursday,
      closingHourFriday,
      closingHourSaturday,
      closingHourSunday,
    } = req.body;

    console.log(req.user);

    if (req.user) {
      // on ne traite que les infos qui ne sont pas à -1
      if (
        isFinite(defaultResaDurationHours) &&
        defaultResaDurationHours > 0 &&
        defaultResaDurationHours <= 24
      ) {
        console.log("ok defaultResaDurationHours est au bon format");

        await Bibliotheque.findByIdAndUpdate(idBib, {
          defaultResaDurationHours: defaultResaDurationHours,
        });
      }
      if (isValidFormatDecimalHours(openingHourMonday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourMonday: openingHourMonday,
        });
      }
      if (isValidFormatDecimalHours(openingHourTuesday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourTuesday: openingHourTuesday,
        });
      }
      if (isValidFormatDecimalHours(openingHourWednesday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourWednesday: openingHourWednesday,
        });
      }
      if (isValidFormatDecimalHours(openingHourThursday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourThursday: openingHourThursday,
        });
      }
      if (isValidFormatDecimalHours(openingHourFriday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourFriday: openingHourFriday,
        });
      }
      if (isValidFormatDecimalHours(openingHourSaturday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourSaturday: openingHourSaturday,
        });
      }
      if (isValidFormatDecimalHours(openingHourSunday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          openingHourSunday: openingHourSunday,
        });
      }
      if (isValidFormatDecimalHours(closingHourMonday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourMonday: closingHourMonday,
        });
      }
      if (isValidFormatDecimalHours(closingHourTuesday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourTuesday: closingHourTuesday,
        });
      }
      if (isValidFormatDecimalHours(closingHourWednesday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourWednesday: closingHourWednesday,
        });
      }
      if (isValidFormatDecimalHours(closingHourThursday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourThursday: closingHourThursday,
        });
      }
      if (isValidFormatDecimalHours(closingHourFriday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourFriday: closingHourFriday,
        });
      }
      if (isValidFormatDecimalHours(closingHourSaturday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourSaturday: closingHourSaturday,
        });
      }
      if (isValidFormatDecimalHours(closingHourSunday)) {
        await Bibliotheque.findByIdAndUpdate(idBib, {
          closingHourSunday: closingHourSunday,
        });
      }

      const response = await Bibliotheque.findById(idBib);
      res.json(response);
    }
    return -1;

    // configBibData : payload
    // on ne traite que les infos qui ne sont pas à -1
  })();
});

function isValidFormatDecimalHours(hour) {
  if (isFinite(hour) && hour <= 24 && hour >= 0) {
    return true;
  }
  return false;
}

module.exports = router;
