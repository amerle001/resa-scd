const express = require("express");
const passport = require("passport");
require("../../config/passportConfig")(passport);
const router = express.Router();

// @route     GET /users/logout
// @desc      logout user
// @access    public

router.get("/", (req, res) => {
  console.log("logout");
  console.log(req.user);

  req.logOut();
  res.send("déconnecté");
  console.log(req.user);

  // res.redirect("/");
});

module.exports = router;
