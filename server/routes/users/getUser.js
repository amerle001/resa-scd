const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  if (req.user) {
    console.log(req.user);
  } else {
    console.log("Pas d'utilisateur authentifié.");
  }

  res.send(req.user); // The req.user stores the entire user that has been authenticated inside of it.
});

module.exports = router;
