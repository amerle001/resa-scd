const express = require("express");
const router = express.Router();

// Bibliotheque et User Models
const Bibliotheque = require("../../models/Bibliotheque");
const User = require("../../models/User");
const ResaLog = require("../../models/ResaLog");

// @route   POST api/reservePlace
// @desc    invoque middleware de Mongoose : Model.updateOne() (findByIdAndUpdate() ne fonctionne pas sur id nichés)
// @access  PUBLIC

router.post("/", (req, res) => {
  (async () => {
    // collation des infos de réservation à partir de la requête frontend
    const { nomReservant, idBib } = req.body;
    const compteTemporaire = req.body.compteTemporaire ? true : false;

    // partie reservations normal (users)
    if (!compteTemporaire) {
      const startResa = Date.now();
      const endResa = startResa + 3600000; // plus 1 heure

      // Vérification que l'usager n'a pas deux resas concomitantes
      // Si tel est le cas, on renvoit la bibliothèque telle quelle.
      const user = await User.findOne({ username: nomReservant });
      for (let userFResa of user.reservations) {
        const userFResaEnd =
          userFResa.start.getTime() + userFResa.durationInMinutes * 60 * 1000;
        if (
          (startResa >= userFResa.start && endResa <= userFResaEnd) ||
          (startResa <= userFResaEnd && endResa >= userFResaEnd) ||
          (startResa <= userFResa.start && endResa >= userFResa.start) ||
          (startResa == userFResa.start && endResa == userFResaEnd)
        ) {
          console.log(
            `Résa immédiate refusée : ${nomReservant} a déjà une autre resa sur créneau.`
          );
          const response = await Bibliotheque.findById(idBib);
          res.json(response);
          return;
        }
      }

      const currentBib = await Bibliotheque.findById(idBib);
      console.log("ma contenance est : ");
      console.log(currentBib.contenance);

      // s'il ne reste plus de place, je renvoie la même bibliothèque
      if (currentBib.contenance <= 0) {
        await Bibliotheque.findByIdAndUpdate(idBib, { contenance: 0 })
          .clone()
          .catch((err) => console.log(err));
        const sameBib = await Bibliotheque.findById(idBib);
        res.json(sameBib);
      } else {
        // création d'une copie de l'état de la salle de la bibliothèque
        const bibRequested = await Bibliotheque.findById(idBib).select(
          "places"
        );
        const currentSalle = await bibRequested.places;

        // création d'une copie temporaire de nouvelle place réservée
        // son id est celui de la première place libre trouvée dans la copie de salle
        let newPlaceReservee = {};
        let idNewPlaceReservee = {};
        for (let currentPlace of currentSalle) {
          if (currentPlace.libre) {
            const start = startResa;
            // 1 mn = 60000 ms
            // 1h = 1000ms * 60s * 60m = 3600000
            const defaultDuree = 3600000;
            idNewPlaceReservee = currentPlace._id;
            newPlaceReservee._id = currentPlace._id;
            newPlaceReservee.nom = nomReservant;
            newPlaceReservee.numero = currentPlace.numero;
            newPlaceReservee.startReservation = start;
            newPlaceReservee.durationInMinutes = defaultDuree;
            newPlaceReservee.endReservation = start + defaultDuree;
            newPlaceReservee.libre = false;
            break;
          }
        }

        // maintenant que la place temporaire a toutes les infos nécessaires
        // update ciblé de la place vide par la copie de la place réservée. Match sur l'id.
        await Bibliotheque.updateOne(
          {
            _id: idBib,
            "places._id": idNewPlaceReservee,
          },
          {
            "places.$.nom": newPlaceReservee.nom,
            "places.$.numero": newPlaceReservee.numero,
            "places.$.startReservation": newPlaceReservee.startReservation,
            "places.$.endReservation": newPlaceReservee.endReservation,
            "places.$.durationInMinutes":
              newPlaceReservee.durationInMinutes / 60000,
            "places.$.libre": false,
          }
        )
          .clone()
          .catch((err) => console.log(err));
        // .then(res.json(bibId, newPlaceReservee));

        // La MaJ de la salle est terminée
        // En suivant, MaJ du nombre de places disponibles de la bib
        // Même méthode : création d'une copie de l'état à jour à partir de l'ancien état
        const prevContenance = await Bibliotheque.findById(idBib).select(
          "contenance"
        );
        const newContenance = (await prevContenance.contenance) - 1;

        // Update direct via findByIdAndUpdate car _id n'est pas niché
        await Bibliotheque.findByIdAndUpdate(
          idBib,
          { contenance: newContenance },
          {
            useFindAndModify: false,
          }
        )
          .clone()
          .catch((err) => console.log(err));

        // Update document usager : has_already_a_seat = true

        const user = await User.findOne({ username: nomReservant });
        await user.reservations.push({
          locationId: idBib,
          placeId: idNewPlaceReservee,
          idNewResaFromPlace: "resaImmediate",
          locationName: currentBib.name,
          placeNumber: newPlaceReservee.numero,
          start: newPlaceReservee.startReservation,
          durationInMinutes: newPlaceReservee.durationInMinutes / 60000,
        });

        await user
          .save()
          .then(() => console.log("Resa has been pushed to user account."));

        await User.findOneAndUpdate(
          { username: nomReservant },
          { has_already_a_seat: true },
          () => console.log("has_already_a_seat: updated")
        )
          .clone()
          .catch((err) => console.log(err));

        // Renvoi de la bibliothèque complète pour update Redux
        const response = await Bibliotheque.findById(idBib);
        res.json(response);

        // log resa
        // const userLogged = await User.findOne({ username: fResa.nom });

        const logEntry = new ResaLog({
          firstName: await user.firstName,
          lastName: await user.lastName,
          username: await user.username,
          location: await response.name,
          placeNumber: await newPlaceReservee.numero,
          startPresence: await newPlaceReservee.startReservation,
          endPresence: await newPlaceReservee.endReservation,
          stayDurationInMinutes:
            (await newPlaceReservee.durationInMinutes) / 60000,
        });

        await logEntry
          .save()
          .then((entry) => console.log(entry))
          .catch((err) => console.log(err));
      }
    } else {
      // ***********************************************
      // ************* COMPTES TEMPORAIRES *************
      // ***********************************************

      // partie reservations immédiates faites par admin
      // avec création de comptes temporaires
      // Procédure comptes temporaires
      // il faut d'abord vérifier si le compte temporaire existe
      // ou sinon, on le créée

      const startResa = Date.now();
      const endResa = startResa + 3600000; // plus 1 heure

      const compteExistant = await User.findOne({ lastName: nomReservant });
      console.log(compteExistant);

      if ((await compteExistant) === null) {
        const compteTemporaire = new User({
          firstName: nomReservant,
          lastName: nomReservant,
          username: `${nomReservant}_${Math.random() * 10000000}`,
          password: `${nomReservant}_${Math.random() * 10000000}`,
          register_date: Date.now(),
          privilege_level: "user",
          is_active: false,
          temporaire: true,
          has_already_a_seat: false,
          reservations: [],
        });
        compteTemporaire
          .save()
          .then(() => console.log(`Compte temporaire ${nomReservant} créé.`));

        // =======================================================
        // verif resa concomittante incluse dans if car sinon
        // user.reservations === null, car recherche asynchrone
        // =======================================================

        // Vérification que l'usager n'a pas deux resas concomitantes
        // Si tel est le cas, on renvoit la bibliothèque telle quelle.
        const user = await User.findOne({ lastName: nomReservant });
        if (await user) {
          for (let userFResa of await user.reservations) {
            const userFResaEnd =
              userFResa.start.getTime() +
              userFResa.durationInMinutes * 60 * 1000;
            if (
              (startResa >= userFResa.start && endResa <= userFResaEnd) ||
              (startResa <= userFResaEnd && endResa >= userFResaEnd) ||
              (startResa <= userFResa.start && endResa >= userFResa.start) ||
              (startResa == userFResa.start && endResa == userFResaEnd)
            ) {
              console.log(
                `Résa immédiate refusée : ${nomReservant} a déjà une autre resa sur créneau.`
              );
              const response = await Bibliotheque.findById(idBib);
              res.json(response);
              return;
            }
          }
        }

        //======================================================
      }

      // Vérification que l'usager n'a pas deux resas concomitantes
      // Si tel est le cas, on renvoit la bibliothèque telle quelle.
      const user = await User.findOne({ lastName: nomReservant });
      for (let userFResa of await user.reservations) {
        const userFResaEnd =
          userFResa.start.getTime() + userFResa.durationInMinutes * 60 * 1000;
        if (
          (startResa >= userFResa.start && endResa <= userFResaEnd) ||
          (startResa <= userFResaEnd && endResa >= userFResaEnd) ||
          (startResa <= userFResa.start && endResa >= userFResa.start) ||
          (startResa == userFResa.start && endResa == userFResaEnd)
        ) {
          console.log(
            `Résa immédiate refusée : ${nomReservant} a déjà une autre resa sur créneau.`
          );
          const response = await Bibliotheque.findById(idBib);
          res.json(response);
          return;
        }
      }

      const currentBib = await Bibliotheque.findById(idBib);
      console.log("ma contenance est : ");
      console.log(currentBib.contenance);

      // s'il ne reste plus de place, je renvoie la même bibliothèque
      if (currentBib.contenance <= 0) {
        await Bibliotheque.findByIdAndUpdate(idBib, { contenance: 0 })
          .clone()
          .catch((err) => console.log(err));
        const sameBib = await Bibliotheque.findById(idBib);
        res.json(sameBib);
      } else {
        // création d'une copie de l'état de la salle de la bibliothèque
        const bibRequested = await Bibliotheque.findById(idBib).select(
          "places"
        );
        const currentSalle = await bibRequested.places;

        // création d'une copie temporaire de nouvelle place réservée
        // son id est celui de la première place libre trouvée dans la copie de salle
        let newPlaceReservee = {};
        let idNewPlaceReservee = {};
        for (let currentPlace of currentSalle) {
          if (currentPlace.libre) {
            const start = startResa;
            // 1 mn = 60000 ms
            // 1h = 1000ms * 60s * 60m = 3600000
            const defaultDuree = 3600000;
            idNewPlaceReservee = currentPlace._id;
            newPlaceReservee._id = currentPlace._id;
            newPlaceReservee.nom = nomReservant;
            newPlaceReservee.numero = currentPlace.numero;
            newPlaceReservee.startReservation = start;
            newPlaceReservee.durationInMinutes = defaultDuree;
            newPlaceReservee.endReservation = start + defaultDuree;
            newPlaceReservee.libre = false;
            break;
          }
        }

        // maintenant que la place temporaire a toutes les infos nécessaires
        // update ciblé de la place vide par la copie de la place réservée. Match sur l'id.
        await Bibliotheque.updateOne(
          {
            _id: idBib,
            "places._id": idNewPlaceReservee,
          },
          {
            "places.$.nom": newPlaceReservee.nom,
            "places.$.numero": newPlaceReservee.numero,
            "places.$.startReservation": newPlaceReservee.startReservation,
            "places.$.endReservation": newPlaceReservee.endReservation,
            "places.$.durationInMinutes":
              newPlaceReservee.durationInMinutes / 60000,
            "places.$.libre": false,
          }
        )
          .clone()
          .catch((err) => console.log(err));
        // .then(res.json(bibId, newPlaceReservee));

        // La MaJ de la salle est terminée
        // En suivant, MaJ du nombre de places disponibles de la bib
        // Même méthode : création d'une copie de l'état à jour à partir de l'ancien état
        const prevContenance = await Bibliotheque.findById(idBib).select(
          "contenance"
        );
        const newContenance = (await prevContenance.contenance) - 1;

        // Update direct via findByIdAndUpdate car _id n'est pas niché
        await Bibliotheque.findByIdAndUpdate(
          idBib,
          { contenance: newContenance },
          {
            useFindAndModify: false,
          }
        )
          .clone()
          .catch((err) => console.log(err));

        // Update document usager : has_already_a_seat = true

        const user = await User.findOne({ lastName: nomReservant });
        await user.reservations.push({
          locationId: idBib,
          placeId: idNewPlaceReservee,
          idNewResaFromPlace: "resaImmediate",
          locationName: currentBib.name,
          placeNumber: newPlaceReservee.numero,
          start: newPlaceReservee.startReservation,
          durationInMinutes: newPlaceReservee.durationInMinutes / 60000,
        });

        await user
          .save()
          .then(() =>
            console.log("Resa has been pushed to user temp account.")
          );

        await User.findOneAndUpdate(
          { lastName: nomReservant },
          { has_already_a_seat: true },
          () => console.log("temp : has_already_a_seat: updated")
        )
          .clone()
          .catch((err) => console.log(err));
        // Renvoi de la bibliothèque complète pour update Redux
        const response = await Bibliotheque.findById(idBib);
        res.json(response);

        // log resa
        const logEntry = new ResaLog({
          firstName: await user.firstName,
          lastName: await user.lastName,
          username: await user.username,
          location: await response.name,
          placeNumber: await newPlaceReservee.numero,
          startPresence: await newPlaceReservee.startReservation,
          endPresence: await newPlaceReservee.endReservation,
          stayDurationInMinutes:
            (await newPlaceReservee.durationInMinutes) / 60000,
        });

        await logEntry
          .save()
          .then((entry) => console.log(entry))
          .catch((err) => console.log(err));
      }
    }
  })();
});

module.exports = router;
