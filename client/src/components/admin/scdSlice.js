import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

// https://redux.js.org/tutorials/essentials/part-5-async-logic

const initialState = {
  scd: [],
  status: "idle",
  error: null,
  bibAffichee: null,
  programResaStatus: null,
  deleteResaStatus: null,
  configureBibStatus: "idle",
  programResaMessage: "",
};

// fetch régulier pour MaJ des salles, trop gourmand en ressources ?
// export const fetchScd = createAsyncThunk("scd/fetchScd", async () => {
//   setInterval(() => {
//     axios
//       .get("/api/bibliotheques", {
//         withCredentials: true,
//       })
//       .then((response) => response.data);
//     // return response.data; // return data car sinon bug "Do Not Put Non-Serializable Values in State or Actions"
//   }, 6000);
// });

export const fetchScd = createAsyncThunk("scd/fetchScd", async () => {
  const response = await axios.get("/api/bibliotheques", {
    withCredentials: true,
  });
  return response.data; // return data car sinon bug "Do Not Put Non-Serializable Values in State or Actions"
});

export const postNewBibliotheque = createAsyncThunk(
  "scd/postNewBibliotheque",
  async (newBib) => {
    const response = await axios.post("/api/bibliotheques", newBib, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const deleteBibliotheque = createAsyncThunk(
  "scd/deleteBibliotheque",
  async (bibId) => {
    console.log("delete bib a pour argument : " + bibId);

    const response = await axios.delete(`/api/bibliotheques/${bibId}`, {
      withCredentials: true,
    });
    console.log(response.data);
    return response.data.bibliothequeDeleted._id;
  }
);

export const reservePlace = createAsyncThunk(
  "scd/reservePlace",
  async (infoResa) => {
    const response = await axios.post("/api/reservePlace", infoResa, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const liberePlace = createAsyncThunk(
  "scd/liberePlace",
  async (infoPlaceLiberee) => {
    const response = await axios.post("/api/liberePlace", infoPlaceLiberee, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const liberePlaceUser = createAsyncThunk(
  "scd/liberePlaceUser",
  async (infoPlaceLiberee) => {
    const response = await axios.post(
      "/api/liberePlaceUser",
      infoPlaceLiberee,
      {
        withCredentials: true,
      }
    );
    return response.data;
  }
);

export const programResa = createAsyncThunk(
  "scd/programResa",
  async (infoProgramResa) => {
    const response = await axios.post("/api/programResa", infoProgramResa, {
      withCredentials: true,
    });
    return response.data;
  }
);
export const deleteResa = createAsyncThunk(
  "scd/deleteResa",
  async (infoProgramResa) => {
    const response = await axios.post("/api/deleteResa", infoProgramResa, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const configureBib = createAsyncThunk(
  "scd/configureBib",
  async (configBibData) => {
    const response = await axios.post("/api/configure", configBibData, {
      withCredentials: true,
    });
    return response.data;
  }
);

export const scdSlice = createSlice({
  name: "scd",
  initialState,
  reducers: {
    afficherBibliotheque: (state, action) => {
      if (state.bibAffichee) {
        state.bibAffichee = null;
      } else {
        const bibId = action.payload;
        const temp = state.scd.filter((bib) => bib._id === bibId);
        const bibTemp = temp[0];
        state.bibAffichee = bibTemp;
      }
    },
    resetProgramResaStatus: (state, action) => {
      if (state.programResaStatus) state.programResaStatus = null;
      if (state.programResaMessage) state.programResaMessage = "";
    },
  },
  extraReducers: {
    [fetchScd.pending]: (state, action) => {
      state.status = "loading";
    },
    [fetchScd.fulfilled]: (state, action) => {
      state.status = "succeeded";
      state.scd = action.payload; // ajout des bibliothèques à l'état
    },
    [fetchScd.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
    [configureBib.pending]: (state, action) => {
      state.configureBibStatus = "loading";
    },
    [configureBib.rejected]: (state, action) => {
      state.configureBibStatus = "failed";
    },
    [postNewBibliotheque.fulfilled]: (state, action) => {
      state.scd.push(action.payload);
    },
    [deleteBibliotheque.fulfilled]: (state, action) => {
      state.scd = state.scd.filter(
        (bibliotheque) => bibliotheque._id !== action.payload
      );
      if (state.bibAffichee) {
        if (action.payload === state.bibAffichee._id) state.bibAffichee = null;
      }
    },
    [reservePlace.fulfilled]: (state, action) => {
      console.log(action.payload);
      state.scd = state.scd.filter((bib) => bib._id !== action.payload._id);
      state.scd.push(action.payload);
    },
    [programResa.fulfilled]: (state, action) => {
      state.scd = state.scd.filter(
        (bib) => bib._id !== action.payload.response._id
      );
      state.scd.push(action.payload.response);
      state.programResaMessage = action.payload.message;

      state.programResaStatus = "succeeded";
    },
    [deleteResa.fulfilled]: (state, action) => {
      state.scd = state.scd.filter((bib) => bib._id !== action.payload._id);
      state.scd.push(action.payload);
      state.deleteResaStatus = "succeeded";
    },
    [liberePlace.fulfilled]: (state, action) => {
      console.log(action.payload);
      state.scd = state.scd.filter((bib) => bib._id !== action.payload._id);
      state.scd.push(action.payload);
    },
    [liberePlaceUser.fulfilled]: (state, action) => {
      console.log(action.payload);
      state.scd = state.scd.filter((bib) => bib._id !== action.payload._id);
      state.scd.push(action.payload);
    },
    [configureBib.fulfilled]: (state, action) => {
      if (action.payload === -1) {
      } else {
        state.scd = state.scd.filter((bib) => bib._id !== action.payload._id);
        state.scd.push(action.payload);
      }
      state.configureBibStatus = "idle";
    },
  },
});

export const {
  afficherBibliotheque,
  resetProgramResaStatus,
} = scdSlice.actions;

export default scdSlice.reducer;

// SELECTORS
export const selectScd = (state) => state.scd.scd;
export const selectBibAffichee = (state) => state.scd.bibAffichee; // ne garder que l'id pour éviter de dupliquer l'état

export const selectCurrentBib = (state) =>
  state.scd.scd.filter((bib) => bib._id === state.scd.bibAffichee._id);
