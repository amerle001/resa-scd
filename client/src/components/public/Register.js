import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import { useSelector, useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  registerUser,
  selectRegisterStatus,
  selectStatusMessage,
} from "../admin/userSlice";
import AlertTimeOut from "./AlertTimeOut";

export default function Register() {
  const dispatch = useDispatch();
  const registerSuccess = useSelector(selectRegisterStatus);
  const registerMessage = useSelector(selectStatusMessage);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");

  const [registerRequestStatus, setRegisterRequestStatus] = useState("idle");

  const validatePasswordStrengh = (string) => {
    return string.length >= 8 ? true : false;
  };

  const samePasswords = () => {
    return password === passwordConfirm ? true : false;
  };

  const canRegister =
    samePasswords &&
    validatePasswordStrengh(password) &&
    [firstName, lastName, username].every(Boolean) &&
    registerRequestStatus === "idle";

  const onRegister = async () => {
    if (canRegister) {
      try {
        setRegisterRequestStatus("pending");
        const resultAction = await dispatch(
          registerUser({ firstName, lastName, username, password })
        );
        unwrapResult(resultAction);
        setFirstName("");
        setLastName("");
        setUsername("");
        setPassword("");
        setPasswordConfirm("");
      } catch (error) {
        console.log("Echec de l'inscription : " + error);
      } finally {
        setRegisterRequestStatus("idle");
      }
    }
  };

  const displayAlert = () => {
    switch (registerSuccess) {
      case "REGISTER_SUCCESS":
        return <AlertTimeOut type="success" message="Inscription réussie !" />;

      case "REGISTER_FAILURE":
        return (
          <AlertTimeOut
            type="danger"
            message={`Echec de l'inscription : ${registerMessage}`}
          />
        );

      default:
        return null;
    }
  };

  return (
    <div>
      {registerRequestStatus === "idle" ? displayAlert() : null}
      <h2>S'inscrire</h2>
      <Form>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Prénom</Form.Label>
            <Form.Control
              type="text"
              name="firstName"
              id="firstName"
              placeholder="Prénom"
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="text"
              name="lastName"
              id="lastName"
              placeholder="Nom"
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>
        </Form.Row>
        <Form.Group controlId="emailRegister">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            name="email"
            placeholder="xxx@yyy.com"
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="registerPassword">
          <Form.Label>Mot de passe</Form.Label>
          <Form.Control
            type="password"
            name="password"
            placeholder="Entrez votre mot de passe"
            onChange={(e) => setPassword(e.target.value)}
          />
          <Form.Text className="text-muted">
            {!validatePasswordStrengh(password) && password.length > 1
              ? "Mot de passe trop court"
              : ""}
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="registerPasswordConfirm">
          <Form.Label>Confirmation du mot de passe</Form.Label>
          <Form.Control
            type="password"
            name="passwordConfirm"
            placeholder="Confirmez votre mot de passe"
            onChange={(e) => setPasswordConfirm(e.target.value)}
          />
          <Form.Text className="text-muted">
            {passwordConfirm.length >= 8 && !samePasswords()
              ? "Les mots de passe ne correspondent pas !"
              : ""}
          </Form.Text>
        </Form.Group>
        <Button
          variant="outline-dark"
          onClick={onRegister}
          disabled={!canRegister}
        >
          S'inscrire
        </Button>
      </Form>
    </div>
  );
}
